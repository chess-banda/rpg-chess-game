


import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/GameConfig.dart';
import 'package:rpgchessgame/CustomColors.dart';
import 'package:rpgchessgame/CustomWidgets.dart';
import 'package:rpgchessgame/Localization.dart';
import 'package:rpgchessgame/main.dart';
import 'package:rpgchessgame/views/changeBackgroundMenu.dart';
import 'package:rpgchessgame/views/playersAddingMenu.dart';

import '../CustomMaterialAppClass.dart';


class StartGame extends StatefulWidget {
  @override
  _StartGameState createState() => _StartGameState();
}

class _StartGameState extends State<StartGame> {
  int playerCount = 2, minPlayerCount = 2, maxPlayerCount = 2;
  DateTime
      timeCount = ProvideGameConfig.getInst().timeOfGame,
      minTimeCount = DateTime(0, 1, 1, 0, 5, 0, 0, 0),
      maxTimeCount = DateTime(0, 1, 1, 0, 50, 0, 0, 0);


  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double fontSize = h * 0.05;
    double buttonH = h * 0.15;
    double buttonW = w * 0.6;

    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Spacer(flex: 4,),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    border: Border.all(
                      color: CustomColors.Main1C,
                      width: 3,
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SimpleBoxButton(
                        onTap: () {setState(_decPlayerCount);},
                        icon: Icon(Icons.remove, color: CustomColors.LightA,),
                        minWidth: buttonH, height: buttonH,
                      ),
                      Spacer(),
                      SimpleText(
                        text: GLocalizations.of(context).startGameMenuTextPlayersCount,
                        color: CustomColors.LightA, size: fontSize,
                      ),
                      SimpleText(
                        text: playerCount.toString(),
                        color: CustomColors.LightA, size: fontSize,
                      ),
                      Spacer(),
                      SimpleBoxButton(
                        onTap: () {setState(_incPlayerCount);},
                        icon: Icon(Icons.add, color: CustomColors.LightA,),
                        minWidth: buttonH, height: buttonH,
                      ),
                    ],
                  ),
                  width: buttonW,
                ),
                Spacer(flex: 1,),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7),
                    border: Border.all(
                      color: CustomColors.Main1C,
                      width: 3,
                    ),
                  ),
                  child: Row(
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SimpleBoxButton(
                        onTap: () {setState(_decTimeCount);},
                        icon: Icon(Icons.remove, color: CustomColors.LightA,),
                        minWidth: buttonH, height: buttonH,
                      ),
                      Spacer(),
                      SimpleText(
                        text: GLocalizations.of(context).startGameMenuTextTimeCount,
                        color: CustomColors.LightA, size: fontSize,
                      ),
                      SimpleText(
                        text: timeCount.minute.toString(),
                        color: CustomColors.LightA, size: fontSize,
                      ),
                      SimpleText(
                        text: GLocalizations.of(context).startGameMenuTextTimeMinutes,
                        color: CustomColors.LightA, size: fontSize,
                      ),
                      Spacer(),
                      SimpleBoxButton(
                        onTap: () {setState(_incTimeCount);},
                        icon: Icon(Icons.add, color: CustomColors.LightA,),
                        minWidth: buttonH, height: buttonH,
                      ),
                    ],
                  ),
                  width: buttonW,
                ),
                Spacer(flex: 1,),
                Container(
                  width: buttonW,
                  height: buttonH,
                  child: SimpleButton(
                    child: FlatButton(
                      onPressed: _procChangeBackground(context),
                      color: CustomColors.Main1C,
                      child: SimpleText(
                        text: GLocalizations.of(context).startGameMenuButtonChangeBackground,
                        color: CustomColors.LightA, size: fontSize,
                      ),
                    ),
                    minWidth: buttonW,
                    height: buttonH,
                  ),
                ),
                Spacer(flex: 1,),
                Container(
                  width: buttonW,
                  height: buttonH,
                  child: SimpleButton(
                    child: FlatButton(
                      onPressed: _procNext(context),
                      color: CustomColors.Main1C,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SimpleText(
                            text: GLocalizations.of(context).startGameMenuButtonConfigPlayers,
                            color: CustomColors.LightA, size: fontSize,
                          ),
                          SimpleText(
                            text: "  ",
                            color: CustomColors.LightA, size: fontSize,
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: CustomColors.LightA,
                          ),
                        ],
                      ),
                    ),
                    minWidth: buttonW,
                    height: buttonH,
                  ),
                ),
                Spacer(flex:4,),
                Row(
                  children: <Widget>[
                    SimpleBoxButton(
                      onTap: _procPrev(context),
                      icon: Icon(Icons.arrow_back, color: CustomColors.LightA,),
                      height: buttonH,
                      minWidth: buttonH,
                    ),
                    Spacer(flex: 1,),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _incPlayerCount() {
    if(playerCount < maxPlayerCount) {
      ++playerCount;
    }
  }
  void _decPlayerCount() {
    if(playerCount > minPlayerCount) {
      --playerCount;
    }
  }

  void _incTimeCount() {
    if(timeCount.compareTo(maxTimeCount) != 0) {
      timeCount = timeCount.add(new Duration(minutes: 5));
    }
  }

  void _decTimeCount() {
    if(timeCount.compareTo(minTimeCount) != 0) {
      timeCount = timeCount.subtract(new Duration(minutes: 5));
    }
  }

  VoidCallback _procNext(BuildContext context) {
    return () {
      ProvideGameConfig.getInst().playersCount = playerCount;
      ProvideGameConfig.getInst().timeOfGame = timeCount;
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => PlayersAddingMenu(),
      )
      );
    };
  }


  VoidCallback _procPrev(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }

  VoidCallback _procChangeBackground(BuildContext context) {
    return () {
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => ChangeBackgroundMenu(),
      )
      );
    };

  }

}





