


import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/GameConfig.dart';


import '../CustomColors.dart';
import '../CustomWidgets.dart';

class ChangeBackgroundMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChangeBackgroundMenuState();
}


class _ChangeBackgroundMenuState extends State<ChangeBackgroundMenu> {
  final List<String> backgrounds = const <String>[
    "assets/gamebackgrounds/black_chess.jpg",
    "assets/gamebackgrounds/black_fone.jpg",
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          Image.asset(
            ProvideGameConfig.getInst().wayToGameBackground,
            alignment: Alignment.center,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            children: <Widget>[
              Container(
                  height: MediaQuery.of(context).size.height / 2,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _drawImage(
                        path: ProvideGameConfig.getInst().wayToBlackCell + "1.jpg",
                        h: MediaQuery.of(context).size.height * 0.3,
                        w: MediaQuery.of(context).size.height * 0.3,
                        onTap: _procCellSelector(context, true),
                        color: Colors.black,
                      ),
                      _drawImage(
                        path: ProvideGameConfig.getInst().wayToWhiteCell + "1.jpg",
                        h: MediaQuery.of(context).size.height * 0.3,
                        w: MediaQuery.of(context).size.height * 0.3,
                        onTap: _procCellSelector(context, false),
                        color: Colors.white
                      ),
                    ],
                  ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 2,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 50),
                        children: <Widget>[
                          for(String path in backgrounds)
                            _drawImage(
                              path: path,
                              h: MediaQuery.of(context).size.height * 0.4,
                              w: MediaQuery.of(context).size.height * 0.8,
                              onTap: () {setState(_procBackgroundSelect(path));},
                              color: CustomColors.Main1C,
                            ),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Spacer(),
                        SimpleBoxButton(
                          height: MediaQuery.of(context).size.height * 0.15,
                          minWidth: MediaQuery.of(context).size.height * 0.15,
                          icon: Icon(
                            Icons.arrow_back,
                            color: CustomColors.LightA,
                          ),
                          onTap: _procPrev(context),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      )
    );
  }

  Widget _drawImage({
    @required String path,
    @required double h,
    @required double w,
    @required Color color,
    @required VoidCallback onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        child: Container(
          height: h,
          width: w,
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10),
          ),
          alignment: Alignment.center,
          child: Image.asset(
            path,
          ),
        ),
      ),
    );
  }

  VoidCallback _procBackgroundSelect(String path) {
    return () {
      ProvideGameConfig.getInst().wayToGameBackground = path;
    };
  }

  VoidCallback _procCellSelector(BuildContext context, bool isBlackCell) {
    return () {
      Navigator.push(context, MaterialPageRoute(
        builder: (context) => CellBackgroundSelector(isBlackCell: isBlackCell,),
      )
      );
    };
  }

  VoidCallback _procPrev(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }
}



class CellBackgroundSelector extends StatelessWidget {
  final List<String> cellBackgrounds = const <String>[
    "assets/cells/beaten_white/",
    "assets/cells/black_red/",
    "assets/cells/blue/",
    "assets/cells/concrete_gray/",
    "assets/cells/concrete_white/",
    "assets/cells/floar_red/",
    "assets/cells/golden/",
    "assets/cells/green/",
    "assets/cells/lava/",
    "assets/cells/light_green/",
    "assets/cells/light_purple/",
    "assets/cells/light_red/",
    "assets/cells/marble_black/",
    "assets/cells/marble_blue/",
    "assets/cells/marble_brown/",
    "assets/cells/marble_green/",
    "assets/cells/marble_light_blue/",
    "assets/cells/marble_orange/",
    "assets/cells/marble_purple/",
    "assets/cells/marble_red/",
    "assets/cells/marble_yellow/",
    "assets/cells/purple/",
    "assets/cells/red/",
    "assets/cells/yellow_one/",
    "assets/cells/yellow_two/",
  ];
  final bool isBlackCell;

  CellBackgroundSelector({@required this.isBlackCell});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: 0,
                horizontal: MediaQuery.of(context).size.height * 0.05),
            child: GridView.count(
              crossAxisCount: 8,
              padding: EdgeInsets.all(5),
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              children: <Widget>[
                for(String path in cellBackgrounds)
                  GestureDetector(
                    onTap: _procSelectCellBackground(context, path),
                    child: Image.asset(path + "1.jpg"),
                  ),
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Spacer(),
              Row(
                children: <Widget>[
                  SimpleBoxButton(
                    minWidth: MediaQuery.of(context).size.height * 0.15,
                    height: MediaQuery.of(context).size.height * 0.15,
                    onTap: () {Navigator.pop(context);},
                    icon: Icon(
                      Icons.arrow_back,
                      color: CustomColors.LightA,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  VoidCallback _procSelectCellBackground(BuildContext context, String path) {
    return () {
      if(isBlackCell) {
        ProvideGameConfig.getInst().wayToBlackCell = path;
      }
      else {
        ProvideGameConfig.getInst().wayToWhiteCell = path;
      }
      Navigator.pop(context);
    };
  }

}

