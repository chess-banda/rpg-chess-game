


import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Cell.dart';
import 'package:rpgchessgame/models/Deck.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';
import 'package:rpgchessgame/views/Game.dart';

import '../CustomColors.dart';
import '../CustomWidgets.dart';
import '../FigureLocalizations.dart';

class CardSelectorInGame extends StatelessWidget {
  final Cell selectedCell;
  final double boxButtonSize = 50;
  final CardSelectAnswer answer;
  final FigureTeam team;

  CardSelectorInGame({
    @required this.selectedCell,
    @required this.answer,
    @required this.team});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          ListView(
            scrollDirection: Axis.horizontal,
            children: buildBody(context),
          ),
          Column(
            children: <Widget>[
              Spacer(),
              Row(
                children: <Widget>[
                  Spacer(),
                  SimpleBoxButton(
                    height: boxButtonSize,
                    minWidth: boxButtonSize,
                    icon: Icon(Icons.arrow_forward, color: CustomColors.Main1C),
                    onTap: _procPrev(context),
                  ),
                ],
              )
            ],
          )
        ],
      )
    );
  }

  List<Widget> buildBody(BuildContext context) {
    List<Widget> allAvailableCards = List<Widget>();
    allAvailableCards.addAll(
        _buildCardsList(
            context: context,
            enumValues: BishopId.values,
            imageGetter: DeckDrawer.getBishopAvatarWtf,
            textGetter: FigureLocalizations.of(context).getBishopName,
            selector: _selectBishopFigure,
        )
    );

    allAvailableCards.addAll(
        _buildCardsList(
            context: context,
            enumValues: KnightId.values,
            imageGetter: DeckDrawer.getKnightAvatarWtf,
            textGetter: FigureLocalizations.of(context).getKnightName,
            selector: _selectKnightFigure,
        )
    );


    allAvailableCards.addAll(
        _buildCardsList(
            context: context,
            enumValues: QueenId.values,
            imageGetter: DeckDrawer.getQueenAvatarWtf,
            textGetter: FigureLocalizations.of(context).getQueenName,
            selector: _selectQueenFigure,
        )
    );

    allAvailableCards.addAll(
        _buildCardsList(
            context: context,
            enumValues: RockId.values,
            imageGetter: DeckDrawer.getRockAvatarWtf,
            textGetter: FigureLocalizations.of(context).getRockName,
            selector: _selectRockFigure,
        )
    );

    return allAvailableCards;
  }

  List<Widget> _buildCardsList({
    @required BuildContext context,
    @required var enumValues,
    @required Widget imageGetter(var id),
    @required String textGetter(var id),
    @required VoidCallback selector(BuildContext context, var id)})
  {
    return [
      for(var id in enumValues)
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.height,
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: selector(context, id),
                child: DeckDrawer.drawCard(
                  image: imageGetter(id),
                  h: MediaQuery.of(context).size.height - boxButtonSize,
                  name: textGetter(id),
                ),
              ),
              Row(
                children: <Widget>[
                  Spacer(),
                  SimpleBoxButton(
                    height: boxButtonSize,
                    minWidth: boxButtonSize,
                    icon: Icon(
                      Icons.check,
                      color: CustomColors.LightA,
                    ),
                    onTap: selector(context, id),
                  ),
                  Spacer(),
                ],
              ),
            ],
          ),
        ),
    ];
  }

  VoidCallback _selectBishopFigure(BuildContext context, dynamic id) {
    return () {
      answer.cardWasSelected = true;
      selectedCell.figure = ProviderAllDecks.getBishopClassFromId(id);
      selectedCell.figure.team = team;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectQueenFigure(BuildContext context, dynamic id) {
    return () {
      answer.cardWasSelected = true;
      selectedCell.figure = ProviderAllDecks.getQueenClassFromId(id);
      selectedCell.figure.team = team;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectRockFigure(BuildContext context, dynamic id) {
    return () {
      answer.cardWasSelected = true;
      selectedCell.figure = ProviderAllDecks.getRockClassFromId(id);
      selectedCell.figure.team = team;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectKnightFigure(BuildContext context, dynamic id) {
    return () {
      answer.cardWasSelected = true;
      selectedCell.figure = ProviderAllDecks.getKnightClassFromId(id);
      selectedCell.figure.team = team;
      Navigator.pop(context);
    };
  }

  VoidCallback _procPrev(BuildContext context) {
    return () {
      answer.cardWasSelected = false;
      Navigator.pop(context);
    };
  }
}


