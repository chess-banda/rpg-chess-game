

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/figures/Config.dart';
import 'package:rpgchessgame/models/figures/pawn/Archer.dart';
import 'package:rpgchessgame/models/figures/pawn/SwordsMen.dart';
import '../models/figures/FigureBase.dart';

import '../Localization.dart';
import '../models/GameConfig.dart';
import '../models/Player.dart';

import '../CustomColors.dart';
import '../CustomWidgets.dart';
import 'Game.dart';
import 'allDecksManagementMenu.dart';

class PlayersAddingMenu extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _PlayersAddingMenuState();
}

class _PlayersAddingMenuState extends State<PlayersAddingMenu> {
  List<Player> _players = <Player>[
    for(int i = 1; i <= ProvideGameConfig.getInst().playersCount; ++i)
      Player(name: "Player " + i.toString()),
  ];

  List<TextEditingController> _textEditingController = <TextEditingController> [
    for(int i = 1; i <= ProvideGameConfig.getInst().playersCount; ++i)
      TextEditingController(text: "Player " + i.toString()),
  ];

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double fontSize = h * 0.05;
    double buttonH = h * 0.15;
    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              for(int i = 0; i < ProvideGameConfig.getInst().playersCount; ++i)
                _buildPlayerEntry(
                    index: i,
                    w: w * 0.9,
                    h: buttonH,
                ),
            ],
          ),
          Column(
            children: <Widget>[
              Spacer(),
              Row(
                children: <Widget>[
                  SimpleBoxButton(
                    onTap: _procPrev(context),
                    icon: Icon(Icons.arrow_back, color: CustomColors.LightA,),
                    height: buttonH,
                    minWidth: buttonH,
                  ),
                  Spacer(flex: 1,),
                  SimpleButton(
                    child: FlatButton(
                      onPressed: _procNext(context),
                      color: CustomColors.Main1C,
                      child: Row(
                        children: <Widget>[
                          SimpleText(
                            text: GLocalizations.of(context).startGameMenuButtonStartGame + "  ",
                            color: CustomColors.LightA,
                            size: fontSize,
                          ),
                          _validateData()
                              ? Icon(Icons.arrow_forward, color: CustomColors.LightA,)
                              : Icon(Icons.close, color: Colors.red,),
                        ],
                      ),
                    ),
                    height: buttonH,
                    minWidth: buttonH * 3,
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildPlayerEntry({
    @required int index,
    @required double w,
    @required double h,
  }) {
    double fontSize = h * 0.3;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
      child: Row(
        children: <Widget>[
          Spacer(),
          Container(
            alignment: Alignment.centerLeft,
            width: w * 0.5,
            height: h,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              color: CustomColors.Main1C,
            ),
            child: EditableText(
              controller: _textEditingController[index],
              onSubmitted: (value) {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                setState(() {_players[index].name = value;});
              },
              onEditingComplete: () {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                setState(() {});
              },
              focusNode: FocusNode(),
              style: TextStyle(
                fontSize: fontSize,
                color: CustomColors.LightA,
              ),
              cursorColor: CustomColors.LightA,
              backgroundCursorColor: CustomColors.LightA,
              selectionColor: CustomColors.Main2E,
            ),
          ),
          Spacer(),
          SimpleButton(
            height: h,
            minWidth: w * 0.3,
            child: FlatButton(
              onPressed: _procSelectDeck(context, index, _players),
              color: CustomColors.Main1C,
              child: Container(
                height: h,
                width: w * 0.3,
                child: Row(
                  children: <Widget>[
                    SimpleText(
                      text: GLocalizations.of(context).playersEditorButtonSelectDeck,
                      color: CustomColors.LightA, size: fontSize,
                    ),
                    Spacer(),
                    Icon(
                      _players[index].selectedDeckIdx != -1
                          ? Icons.check
                          : Icons.close,
                      color: _players[index].selectedDeckIdx != -1
                          ? Colors.green
                          : Colors.red,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          SimpleBoxButton(
            height: h,
            minWidth: h,
            icon: Icon(
              Icons.flag,
              color: _getColorFromTeamId(
                  _players[index].teamId,
              ),
            ),
            onTap:() {setState(_procNextTeamId(
                _players[index].teamId,
                index
            ));},
          ),
          Spacer(),
        ],
      ),
    );

  }




  VoidCallback _procSelectDeck(
      BuildContext context,
      int index,
      List<Player> players) {
    return () {
      Navigator.push(
          context, MaterialPageRoute(
        builder: (context) => AllDecksManagementMenu(
          selectMode: true,
          playerIndex: index,
          players: players,
        ),
      )
      );
    };
  }

  VoidCallback _procNext(BuildContext context) {
    return () {
      if(!_validateData()) {
        Fluttertoast.cancel();
        Fluttertoast.showToast(
          msg: (_validateDeck() ? "" : GLocalizations.of(context).startGameMenuTextChooseDecks)
              + " " +
              (_validateTeam() ? "" : GLocalizations.of(context).startGameMenuTextChooseTeamColor),
          fontSize: MediaQuery.of(context).size.height * 0.05,
          backgroundColor: CustomColors.Main1C,
          gravity: ToastGravity.CENTER,
          textColor: CustomColors.LightA,
          toastLength: Toast.LENGTH_LONG,
        );
        return;
      }

      for(var player in _players) {
        player.figures = ProviderAllDecks
            .getInst()
            .deckToFigureArray(player.selectedDeckIdx);

        for(var type in FigureBaseType.values) {
          for(var figure in player.figures[type]) {
            figure.team = player.teamId;

            if(figure is IPawn) {
              if(figure is Archer) {
                ArcherConfig config = ProvideFiguresConfig.archerConfig;
                switch(figure.team) {
                  case FigureTeam.Black:
                    figure.moves = config.directedMoves[Direction.down];
                    break;
                  case FigureTeam.White:
                    figure.moves = config.directedMoves[Direction.up];
                    break;
                  case FigureTeam.Red:
                    figure.moves = config.directedMoves[Direction.left];
                    break;
                  case FigureTeam.Green:
                    figure.moves = config.directedMoves[Direction.right];
                    break;
                }
              }
              if(figure is SwordsMen) {
                SwordsMenConfig config = ProvideFiguresConfig.swordsMenConfig;
                switch(figure.team) {
                  case FigureTeam.Black:
                    figure.moves = config.directedMoves[Direction.down];
                    break;
                  case FigureTeam.White:
                    figure.moves = config.directedMoves[Direction.up];
                    break;
                  case FigureTeam.Red:
                    figure.moves = config.directedMoves[Direction.left];
                    break;
                  case FigureTeam.Green:
                    figure.moves = config.directedMoves[Direction.right];
                    break;
                }
              }
            }
          }
        }
      }


      Navigator.push(
          context, MaterialPageRoute(
        builder: (context) => Game(players: _players,),
      )
      );

    };
  }


  VoidCallback _procPrev(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }


  bool _validateData() {
    return _validateDeck() && _validateTeam();
  }

  bool _validateDeck() {
    for(int i = 0; i < _players.length; ++i) {
      if(_players[i].selectedDeckIdx == -1)
        return false;
    }

    return true;
  }

  bool _validateTeam() {
    for(int i = 0; i < _players.length; ++i) {
      for(int ii = 0; ii < _players.length; ++ii) {
        if(i != ii && _players[i].teamId == _players[ii].teamId)
          return false;
      }
    }

    return true;
  }


  Color _getColorFromTeamId(FigureTeam teamId) {
    switch(teamId) {
      case FigureTeam.Black: return Colors.black;
      case FigureTeam.White: return Colors.white;
      case FigureTeam.Red: return Colors.red;
      case FigureTeam.Green: return Colors.green;
    }
  }


  VoidCallback _procNextTeamId(FigureTeam teamId, int index) {
    return () {
      switch(teamId) {
        case FigureTeam.Black:
          _players[index].teamId = FigureTeam.White;
          break;
        case FigureTeam.White:
          if(_players.length > 2) _players[index].teamId = FigureTeam.Red;
          else _players[index].teamId = FigureTeam.Black;
          break;
        case FigureTeam.Red:
          if(_players.length > 3) _players[index].teamId = FigureTeam.Green;
          else _players[index].teamId = FigureTeam.Black;
          break;
        case FigureTeam.Green:
          _players[index].teamId = FigureTeam.Black;
          break;
      }
    };
  }
}


