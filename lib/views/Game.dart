




import 'dart:async';
import 'dart:math' as math;
import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rpgchessgame/CustomColors.dart';
import 'package:rpgchessgame/CustomWidgets.dart';
import 'package:rpgchessgame/Localization.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Cell.dart';
import 'package:rpgchessgame/models/Field.dart';
import 'package:rpgchessgame/models/FieldTemplate.dart';
import 'package:rpgchessgame/models/GameConfig.dart';
import 'package:rpgchessgame/models/Player.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';
import 'package:rpgchessgame/models/figures/knight/Rider.dart';
import 'package:rpgchessgame/views/cardSelector.dart';

import '../FigureLocalizations.dart';

enum Exit{
  Yes,
  No
}

class CardSelectAnswer {
  bool cardWasSelected = false;
}

class Game extends StatefulWidget {
  final List<Player> players;

  Game({@required this.players}) {
    for(int i = 0; i < players.length; ++i)
      players[i].timer
      = DateTime.fromMillisecondsSinceEpoch(ProvideGameConfig.getInst().timeOfGame.millisecondsSinceEpoch);
  }
  @override
  State<StatefulWidget> createState() => _GameState(players: players);
}

class _GameState extends State<Game> {
  CardSelectAnswer answer = CardSelectAnswer();
  Field field;
  List<Player> players;
  bool figureSelected = false;
  bool drawFigureInfo = false;
  bool endGameFlag = false;
  bool timeLimitExceeded = false;
  bool allFiguresDestroyed = false;
  bool kingDestroyed = false;
  Cell selectedCell;
  FigureTeam currentTeam = FigureTeam.White;
  FigureTeam winnerTeam;
  Timer timer;
  DateTime futureTime;
  DateTime remainingTime;


  _GameState({@required this.players})
      : field = Field.fromTemplate(
      template: DefaultField(),
      players: players
  );

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 1), (_) {
      if (this.mounted)setState(() {});
    });
    for(var player in players) {
      if(player.teamId == currentTeam) {
        futureTime = DateTime.now().add(
            Duration(
                milliseconds: player.timer.millisecondsSinceEpoch
            )
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    remainingTime = futureTime.subtract(
        Duration(milliseconds: DateTime.now().millisecondsSinceEpoch)
    );


    if(answer.cardWasSelected) {
      currentTeam = _getNextTeamAndUpdateTimer(currentTeam);
      answer.cardWasSelected = false;
    }
    _procEndGame();


    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double buttonH = h * 0.15;
    double fontSize = h * 0.033;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Image.asset(
            ProvideGameConfig.getInst().wayToGameBackground,
            alignment: Alignment.center,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Spacer(),
                  Container(
                    height: h,
                    width: h,
                    child: field.drawField(),
                  ),
                ],
              )
            ],
          ),
          Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Spacer(),
                  Container(
                    height: h,
                    width: h,
                    child: _drawTapGrid(context),
                  ),
                ],
              ),
            ],
          ),
          if(drawFigureInfo && selectedCell.figure != null)
            drawInfoOnField(
              context: context,
              figure: selectedCell.figure,
            ),

          Column(
            children: <Widget>[
              Spacer(),
              Container(
                alignment: Alignment.center,
                width: w - h,
                height: h * 0.15,
                decoration: BoxDecoration(
                  color: CustomColors.Main1C,
                ),
                child: SimpleText(
                  color: CustomColors.LightA,
                  size: fontSize,
                  text: GLocalizations.of(context).gameTimerText
                      + (remainingTime.minute % 60).toString() +
                  ":" + (remainingTime.second % 60).toString(),
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Spacer(),
              SimpleBoxButton(
                height: buttonH,
                minWidth: buttonH,
                icon: Transform.rotate(
                  angle: math.pi,
                  child: Icon(Icons.exit_to_app, color: CustomColors.LightA,),),
                onTap: _procExit(context),
              ),
            ],
          ),
          if(endGameFlag)
            Container(
              color: Color.fromRGBO(20, 20, 20, 70),
              height: h,
              width: w,
              child: Center(
                child: Column (
                  children: <Widget>[
                    Spacer(),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: CustomColors.Main1C,
                          width: 5,
                        ),
                      ),
                      alignment: Alignment.center,
                      height: h * 0.4,
                      width: w / 1.5,
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Spacer(),
                            SimpleText(
                              color: CustomColors.LightA,
                              size: fontSize,
                              text: timeLimitExceeded
                                  ? GLocalizations.of(context).gameTimeLimitExceededText
                                  : (allFiguresDestroyed
                                  ? GLocalizations.of(context).gameAllFiguresDestroyedText
                                  : (kingDestroyed
                                  ? GLocalizations.of(context).gameKingDestroyedText
                                  : "")),
                            ),
                            Spacer(),
                            SimpleText(
                              color: CustomColors.LightA,
                              size: fontSize,
                              text: GLocalizations.of(context).gameWinText
                                  + winnerTeam.toString().split('.').removeLast(),
                            ),
                            Spacer(),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    SimpleBoxButton(
                      height: buttonH,
                      minWidth: buttonH * 4,
                      icon: Icon(Icons.check, color: CustomColors.LightA,),
                      onTap: _procEndGameExit(context),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  Widget _drawTapGrid(BuildContext context) {
    return GridView.count(
      crossAxisCount: field.maxW,
      physics: NeverScrollableScrollPhysics(),
      padding: EdgeInsets.all(0),
      scrollDirection: Axis.vertical,
      children: <Widget>[
        for(int y = 0; y < field.maxH; ++y)
          for(int x = 0; x < field.maxW; ++x)
            GestureDetector(
              onTap: () {setState(_procTap(x, y, context));},
            ),
      ],
    );
  }

  void _procCardSelect(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(
      builder: (context) => CardSelectorInGame(
        selectedCell: selectedCell,
        answer: answer,
        team: currentTeam,
      )
    )
    );
  }

  VoidCallback _procTap(int x, int y, BuildContext context) {
    return () {
        if(figureSelected) {
          _procMoveFigure(x, y);
        } else {
          if (field.field[y][x].figure != null) {
            selectedCell = field.field[y][x];
            drawFigureInfo = true;
            if (currentTeam == field.field[y][x].figure.team) {
              _procSelectFigure(x, y, context);
            }
          } else {
            drawFigureInfo = false;
            figureSelected = false;
            field.movesMap = [
              for(int i = 0; i < field.maxH; ++i)
                List.filled(field.maxW, MoveT.cannot),
            ];
          }
        }

    };
  }

  VoidCallback _procEndGameExit(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }

  VoidCallback _procExit(BuildContext context) {
    return () async {
      double fontSize = MediaQuery.of(context).size.height * 0.06;
      switch(await showDialog<Exit>(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              backgroundColor: CustomColors.Main1C,
              title: Center(
                child: SimpleText(
                  text: GLocalizations.of(context).mainMenuTextExitDialogTitle,
                  color: CustomColors.LightA,
                  size: fontSize,
                ),
              ),
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Spacer(flex: 1,),
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Exit.Yes);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).mainMenuTextExitDialogYes,
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ),
                    Spacer(flex: 1,),
                    SimpleDialogOption(
                      onPressed: () {
                        Navigator.pop(context, Exit.No);
                      },
                      child: SimpleText(
                        text: GLocalizations.of(context).mainMenuTextExitDialogNo,
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ),
                    Spacer(flex: 1,),
                  ],
                ),

              ],
            );
          })) {
        case Exit.Yes:
          {
            Navigator.pop(context);

          }
          break;
        case Exit.No:
          {

          }
          break;
      }
    };
  }

  void _procSelectFigure(int x, int y, BuildContext context) {
    field.movesMap = [
      for(int i = 0; i < field.maxH; ++i)
        List.filled(field.maxW, MoveT.cannot),
    ];

    Point p;
    FigureBase figure;
    Cell nextCell;
    selectedCell = field.field[y][x];
    figure = selectedCell.figure;
    if(figure == null) {
      figureSelected = false;
      return;
    }




    field.movesMap[y][x] = MoveT.figure;
    figureSelected = true;
    for(int i = 0; i < figure.moves.length; ++i) {
      p = figure.moves[i];
      if(x + p.x >= field.maxW
          || x + p.x < 0
          || y + p.y >= field.maxH
          || y + p.y < 0)
        continue;

      nextCell = field.field[y+p.y][x+p.x];
      if(nextCell.figure == null) {
        field.movesMap[y+p.y][x+p.x] = MoveT.can;
      } else {
        field.movesMap[y+p.y][x+p.x] = MoveT.atack;
      }
    }


    bool blockMove = false;
    var _procBlocking = (int _x, int _y) {
      if(field.movesMap[_y][_x] == MoveT.cannot) return false;
      if(!blockMove && field.field[_y][_x].figure != null) {
        blockMove = true;
        if(field.field[y][x].figure.team
            == field.field[_y][_x].figure.team)
          field.movesMap[_y][_x] = MoveT.cannot;
        return true;
      }
      if(blockMove) field.movesMap[_y][_x] = MoveT.cannot;
      return true;
    };


    for(int i = x + 1; i < field.maxW; ++i) {
      if(_procBlocking(i, y)) continue;
      else break;
    }

    blockMove = false;
    for(int i = x - 1; i >= 0; --i) {
      if(_procBlocking(i, y)) continue;
      else break;
    }

    blockMove = false;
    for(int i = y + 1; i < field.maxH; ++i) {
      if(_procBlocking(x, i)) continue;
      else break;
    }

    blockMove = false;
    for(int i = y - 1; i >= 0; --i) {
      if(_procBlocking(x, i)) continue;
      else break;
    }

    blockMove = false;
    for(int ix = x + 1, iy = y + 1;
    ix < field.maxW && iy < field.maxH;
    ++ix, ++iy) {
      if(_procBlocking(ix, iy)) continue;
      else break;
    }

    blockMove = false;
    for(int ix = x - 1, iy = y - 1;
    ix >= 0 && iy >= 0;
    --ix, --iy) {
      if(_procBlocking(ix, iy)) continue;
      else break;
    }

    blockMove = false;
    for(int ix = x - 1, iy = y + 1;
    ix >= 0 && iy < field.maxH;
    --ix, ++iy) {
      if(_procBlocking(ix, iy)) continue;
      else break;
    }

    blockMove = false;
    for(int ix = x + 1, iy = y - 1;
    ix < field.maxW && iy >= 0;
    ++ix, --iy) {
      if(_procBlocking(ix, iy)) continue;
      else break;
    }


    if(figure is IPawn) {
      for(int i = 0; i < figure.moves.length; ++i) {
        p = figure.moves[i];

        if(x + p.x >= field.maxW
            || x + p.x < 0
            || y + p.y >= field.maxH
            || y + p.y < 0)
          continue;

        if(field.field[p.y + y][p.x + x].figure != null && i == 0) {
          field.movesMap[p.y + y][p.x + x] = MoveT.cannot;
        }
        if(!figure.firstMove && i == 1) {
          field.movesMap[p.y + y][p.x + x] = MoveT.cannot;
        }
        if(field.field[p.y + y][p.x + x].figure == null && i > 1) {
          field.movesMap[p.y + y][p.x + x] = MoveT.cannot;
        }

        if(field.field[p.y + y][p.x + x].figure != null) {
          if(field.field[y][x].figure.team
              == field.field[p.y + y][p.x + x].figure.team)
            field.movesMap[p.y + y][p.x + x] = MoveT.cannot;
        }
      }
    }


    if(figure is Rider) {
      for(int i = 0; i < figure.moves.length; ++i) {
        p = figure.moves[i];
        if(x + p.x >= field.maxW
            || x + p.x < 0
            || y + p.y >= field.maxH
            || y + p.y < 0
            || field.field[p.y + y][p.x + x].figure == null)
          continue;


        if(field.field[y][x].figure.team
            == field.field[p.y + y][p.x + x].figure.team)
          field.movesMap[p.y + y][p.x + x] = MoveT.cannot;
      }
      return;
    }

    if(selectedCell.figure is IPawn
        && selectedCell.figure.team == currentTeam) {
      if((y == field.maxH && selectedCell.figure.team == FigureTeam.Black)
          || (y == 0 && selectedCell.figure.team == FigureTeam.White)) {
        answer.cardWasSelected = false;
        _procCardSelect(context);
      }
    }

  }

  void _procMoveFigure(int x, int y) {

    if(selectedCell.figure is IKing) {
      if((selectedCell.figure as IKing).firstMove
          && field.field[y][x].figure is IRock
          && (field.field[y][x].figure as IRock).firstMove) {
        bool doReplace = false;

        if(x == 0) {
          for(int i = 1; i < field.maxW; ++i) {
            if(field.field[y][i].figure != null) {
              if(field.field[y][i].figure is IKing) {
                doReplace = true;
              } else {
                break;
              }
            }
          }
        } else
          {
          for(int i = x - 1; i >= 0; --i) {
            if(field.field[y][i].figure != null) {
              if(field.field[y][i].figure is IKing) {
                doReplace = true;
              } else {
                break;
              }
            }
          }
        }
        if(doReplace) {
          (field.field[y][x].figure as IRock).firstMove = false;
          (selectedCell.figure as IKing).firstMove = false;

          var figure = field.field[y][x].figure;
          field.field[y][x].figure = selectedCell.figure;
          selectedCell.figure = figure;

          currentTeam = _getNextTeamAndUpdateTimer(selectedCell.figure.team);

          figureSelected = false;
          field.movesMap = [
            for(int i = 0; i < field.maxH; ++i)
              List.filled(field.maxW, MoveT.cannot),
          ];
          return;
        }
      }
    }
    if(field.movesMap[y][x] == MoveT.can) {
      field.field[y][x].figure = selectedCell.figure;
      if(selectedCell.figure is IPawn)
        (selectedCell.figure as IPawn).firstMove = false;
      if(selectedCell.figure is IKing)
        (selectedCell.figure as IKing).firstMove = false;
      if(selectedCell.figure is IRock)
        (selectedCell.figure as IRock).firstMove = false;
      currentTeam = _getNextTeamAndUpdateTimer(selectedCell.figure.team);
      selectedCell.figure = null;
    }
    if(field.movesMap[y][x] == MoveT.atack) {
      var damage = selectedCell.figure.atackDamage;
      var targetCell = field.field[y][x];
      if(targetCell.figure.armor > 0) {
        if(damage >= targetCell.figure.armor) {
          damage -= targetCell.figure.armor;
          targetCell.figure.armor = 0;
        } else {
          targetCell.figure.armor -= damage;
          damage = 0;
        }
      } else if (damage > 0){
        if(damage >= targetCell.figure.health) {
          damage -= targetCell.figure.health;
          targetCell.figure.health = 0;
        } else {
          targetCell.figure.health -= damage;
          damage = 0;
        }
      }
      if(targetCell.figure.health == 0) {
        if(selectedCell.figure is IPawn)
          (selectedCell.figure as IPawn).firstMove = false;
        if(selectedCell.figure is IKing)
          (selectedCell.figure as IKing).firstMove = false;
        if(selectedCell.figure is IRock)
          (selectedCell.figure as IRock).firstMove = false;

        targetCell.figure.status = FigureStatus.Killed;
        targetCell.figure = selectedCell.figure;
        currentTeam = _getNextTeamAndUpdateTimer(selectedCell.figure.team);
        selectedCell.figure = null;
      } else {
        currentTeam = _getNextTeamAndUpdateTimer(selectedCell.figure.team);
      }
    }
    figureSelected = false;
    field.movesMap = [
      for(int i = 0; i < field.maxH; ++i)
        List.filled(field.maxW, MoveT.cannot),
    ];
  }

  void _procEndGame() {
    if(remainingTime.minute % 60 == 0
        && remainingTime.second % 60 < 1)
      {
        endGameFlag = true;
        timeLimitExceeded = true;
        winnerTeam = _getNextTeamAndUpdateTimer(currentTeam);
        timer.cancel();
        return;
      }

    for(var player in players) {
      if(player.figures[FigureBaseType.King][0].status
          == FigureStatus.Killed) {
        endGameFlag = true;
        winnerTeam = _getNextTeamAndUpdateTimer(player.teamId);
        kingDestroyed = true;
        timer.cancel();
      }
      bool allDestroyed = true;
      for(var type in FigureBaseType.values) {
        for(var figure in player.figures[type]) {
          if(figure.status != FigureStatus.Killed
              && allDestroyed) {
            allDestroyed = false;
            break;
          }
        }
      }
      if(allDestroyed) {
        endGameFlag = true;
        allFiguresDestroyed = true;
        winnerTeam = _getNextTeamAndUpdateTimer(currentTeam);
        timer.cancel();
      }
    }

  }

  FigureTeam _getNextTeamAndUpdateTimer(FigureTeam team) {
    for(var player in players) {
      if(player.teamId == currentTeam) {
        player.timer = DateTime.fromMillisecondsSinceEpoch(
          remainingTime.millisecondsSinceEpoch
        );
      }
    }
    FigureTeam nextTeam;
    switch(team) {
      case FigureTeam.Black:
        nextTeam = FigureTeam.White;
        break;
      case FigureTeam.White:
        if(players.length > 2) nextTeam = FigureTeam.Red;
        else nextTeam = FigureTeam.Black;
        break;
      case FigureTeam.Red:
        if(players.length > 3) nextTeam = FigureTeam.Green;
        else nextTeam = FigureTeam.Black;
        break;
      case FigureTeam.Green:
        nextTeam = FigureTeam.Black;
        break;
    }

    for(var player in players) {
      if(player.teamId == nextTeam) {
        futureTime = DateTime.now().add(
            Duration(
                milliseconds: player.timer.millisecondsSinceEpoch
            )
        );
      }
    }
    return nextTeam;
  }


}


Widget drawInfoOnField({
  BuildContext context,
  FigureBase figure,
}) {
  int imgScale = 20;
  double h = MediaQuery.of(context).size.height;
  double w = MediaQuery.of(context).size.width;
  return Container(
    color: Color.fromRGBO(90, 90, 90, 160),
    height: h,
    width: w - h,
    child: Column(
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          width: w - h,
          height: h * 0.1,
          decoration: BoxDecoration(
            color: CustomColors.Main1C,
          ),
          child: SimpleText(
            color: CustomColors.LightA,
            size: h * 0.07,
            text: FigureLocalizations.of(context).getFigureName(figure.id),
          ),
        ),
        Row(
          children: <Widget>[

            Container(
              height: h / 2.5,
              width: h / 2.5,
              child: DeckDrawer.getFigureAvatarWtf(figure.id),
            ),
            Column(
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        for(int h = 0; h < figure.maxHPAndArmor; ++h)
                          Image.asset(
                            "assets/icons/hp/health_point_back.png",
                            height: MediaQuery.of(context).size.height / imgScale,
                            width: MediaQuery.of(context).size.height / imgScale,
                            fit: BoxFit.cover,
                          ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        for(int h = 0; h < figure.health; ++h)
                          Image.asset(
                            "assets/icons/hp/health_point.png",
                            height: MediaQuery.of(context).size.height / imgScale,
                            width: MediaQuery.of(context).size.height / imgScale,
                            fit: BoxFit.cover,
                          ),
                      ],
                    ),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        for(int h = 0; h < figure.maxHPAndArmor; ++h)
                          Image.asset(
                            "assets/icons/hp/armor_back.png",
                            height: MediaQuery.of(context).size.height / imgScale,
                            width: MediaQuery.of(context).size.height / imgScale,
                            fit: BoxFit.cover,
                          ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        for(int h = 0; h < figure.armor; ++h)
                          Image.asset(
                            "assets/icons/hp/armor.png",
                            height: MediaQuery.of(context).size.height / imgScale,
                            width: MediaQuery.of(context).size.height / imgScale,
                            fit: BoxFit.cover,
                          ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/icons/damage/atack.png",
                      height: MediaQuery.of(context).size.height / imgScale,
                      width: MediaQuery.of(context).size.height / imgScale,
                      fit: BoxFit.cover,
                    ),
                    SimpleText(
                      color: CustomColors.LightA,
                      size: MediaQuery.of(context).size.height / imgScale,
                      text: figure.atackDamage.toString(),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
        Container(
          alignment: Alignment.center,
          width: w - h,
          height: h * 0.1,
          decoration: BoxDecoration(
            color: CustomColors.Main1C,
          ),
          child: SimpleText(
            color: CustomColors.LightA,
            size: h * 0.04,
            text: GLocalizations.of(context).gameAbilityText,
          ),
        ),
        Row(
          children: <Widget>[
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.height / 5,
              height: MediaQuery.of(context).size.height / 5,
              child: Image.asset("assets/game/yellow.png"),
            ),
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.height / 5,
              height: MediaQuery.of(context).size.height / 5,
              child: Image.asset("assets/game/yellow.png"),
            ),
            Spacer(),
            Container(
              width: MediaQuery.of(context).size.height / 5,
              height: MediaQuery.of(context).size.height / 5,
              child: Image.asset("assets/game/yellow.png"),
            ),
            Spacer(),
          ],
        )
      ],
    ),
  );
}