

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:rpgchessgame/CustomColors.dart';
import 'package:rpgchessgame/CustomWidgets.dart';
import 'package:rpgchessgame/FigureLocalizations.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Deck.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';


import '../Localization.dart';

class DeckManagementMenu extends StatefulWidget {
  final int _selectedIdx;
  DeckManagementMenu(this._selectedIdx);

  @override
  State<StatefulWidget> createState() => _DeckManagementMenuState(_selectedIdx);
}

class _DeckManagementMenuState extends State<DeckManagementMenu> {

  final int _selectedIdx;
  Deck _selectedDeck;
  TextEditingController _textEditingController;

  _DeckManagementMenuState(this._selectedIdx) :
        _selectedDeck =  ProviderAllDecks.getInst().decks[_selectedIdx].clone() {
    _textEditingController = TextEditingController(
      text: _selectedDeck.name,
    );
  }

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double buttonH = h * 0.15;
    double fontSize = h * 0.06;

    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          Container(
            height: h / 2,
            width: w,
            child: DeckDrawer.drawDeckGridRedo(_selectedDeck, h / 4),
          ),
          Container(
            height: h / 2,
            width: w,
            child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 8,
              padding: EdgeInsets.all(3),
              mainAxisSpacing: 3,
              crossAxisSpacing: 3,
              children: <Widget>[
                for(int i = 0; i < 16; ++i)
                  GestureDetector(
                    onTap: () {
                      var builder;
                      if(i < 8) builder = (context) => CardSelector.pawn(i, _selectedDeck);
                      else if(i == 8) builder = (context) => CardSelector.rock(0, _selectedDeck);
                      else if(i == 9) builder = (context) => CardSelector.knight(0, _selectedDeck);
                      else if(i == 10) builder = (context) => CardSelector.bishop(0, _selectedDeck);
                      else if(i == 11) builder = (context) => CardSelector.king(0, _selectedDeck);
                      else if(i == 12) builder = (context) => CardSelector.queen(0, _selectedDeck);
                      else if(i == 13) builder = (context) => CardSelector.bishop(1, _selectedDeck);
                      else if(i == 14) builder = (context) => CardSelector.knight(1, _selectedDeck);
                      else if(i == 15) builder = (context) => CardSelector.rock(1, _selectedDeck);
                      Navigator.push(
                          context, MaterialPageRoute(
                        builder: builder,
                      )
                      );
                    },
                  ),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Spacer(flex: 4),
                Row(
                  children: <Widget>[
                    Spacer(flex: 20,),
                    SimpleText(
                      text: GLocalizations.of(context).deckManagementMenuTextEditName,
                      color: CustomColors.LightA,
                      size: fontSize,
                    ),
                    Spacer(),
                    Container(
                      width: w * 0.4,
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        color: CustomColors.Main1C,
                      ),
                      child: EditableText(
                        controller: _textEditingController,
                        onSubmitted: (value) {
                          SystemChannels.textInput.invokeMethod('TextInput.hide');
                          setState(() {_selectedDeck.name = value;});
                        },
                        onEditingComplete: () {
                          SystemChannels.textInput.invokeMethod('TextInput.hide');
                          setState(() {});
                        },
                        focusNode: FocusNode(),
                        style: TextStyle(
                          fontSize: fontSize,
                          color: CustomColors.LightA,
                        ),
                        cursorColor: CustomColors.LightA,
                        backgroundCursorColor: CustomColors.LightA,
                        selectionColor: CustomColors.Main2E,
                      ),
                    ),
                    Spacer(flex: 20,),
                  ],
                ),
                Spacer(),
                Row(
                  children: <Widget>[
                    SimpleBoxButton(
                      onTap: _procPrevSave(context, _selectedIdx, _selectedDeck),
                      icon: Icon(Icons.check, color: CustomColors.LightA,),
                      minWidth: buttonH,
                      height: buttonH,
                    ),
                    Spacer(),
                    SimpleBoxButton(
                      onTap: _procPrevDiscard(context),
                      icon: Icon(Icons.close, color: CustomColors.LightA,),
                      minWidth: buttonH,
                      height: buttonH,
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}



class CardSelector extends StatefulWidget {
  final FigureBaseType _type;
  final int _selectedCard;
  final Deck _selectedDeck;

  CardSelector.bishop(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Bishop;
  CardSelector.king(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.King;
  CardSelector.knight(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Knight;
  CardSelector.pawn(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Pawn;
  CardSelector.queen(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Queen;
  CardSelector.rock(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Rock;

  @override
  State<StatefulWidget> createState() {
    switch(_type) {
      case FigureBaseType.Bishop:
        return _CardSelectorState.bishop(_selectedCard, _selectedDeck);
        break;
      case FigureBaseType.King:
        return _CardSelectorState.king(_selectedCard, _selectedDeck);
        break;
      case FigureBaseType.Knight:
        return _CardSelectorState.knight(_selectedCard, _selectedDeck);
        break;
      case FigureBaseType.Pawn:
        return _CardSelectorState.pawn(_selectedCard, _selectedDeck);
        break;
      case FigureBaseType.Queen:
        return _CardSelectorState.queen(_selectedCard, _selectedDeck);
        break;
      case FigureBaseType.Rock:
        return _CardSelectorState.rock(_selectedCard, _selectedDeck);
        break;
    }
  }
}

class _CardSelectorState extends State<CardSelector> {
  final FigureBaseType _type;
  final int _selectedCard;
  final Deck _selectedDeck;

  bool showInfo = false;
  dynamic figureSubTupe;

  _CardSelectorState.bishop(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Bishop;
  _CardSelectorState.king(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.King;
  _CardSelectorState.knight(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Knight;
  _CardSelectorState.pawn(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Pawn;
  _CardSelectorState.queen(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Queen;
  _CardSelectorState.rock(this._selectedCard, this._selectedDeck) : _type = FigureBaseType.Rock;
  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double buttonH = h * 0.15;
    return Scaffold(
      backgroundColor: CustomColors.DarkA,
      body: Stack(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: buttonH),
            scrollDirection: Axis.horizontal,
            children: buildBody(context),
          ),
          Column(
            children: <Widget>[
              Spacer(),
              Row(
                children: <Widget>[
                  SimpleBoxButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: CustomColors.LightA,
                    ),
                    minWidth: buttonH,
                    height: buttonH,
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                  Spacer(),
                ],
              ),
            ],
          ),
          if(showInfo)
            _drawFigureInfo(context: context),
        ],
      ),
    );
  }

  List<Widget> buildBody(BuildContext context) {
    switch (_type) {
      case FigureBaseType.Bishop:
        return _buildCardsList(
            context: context,
            enumValues: BishopId.values,
            imageGetter: DeckDrawer.getBishopAvatarWtf,
            textGetter: FigureLocalizations.of(context).getBishopName,
            selector: _selectBishopFigure,
            shower: _showFigureInfo);
        break;
      case FigureBaseType.King:
        return _buildCardsList(
            context: context,
            enumValues: KingId.values,
            imageGetter: DeckDrawer.getKingAvatarWtf,
            textGetter: FigureLocalizations.of(context).getKingName,
            selector: _selectKingFigure,
            shower: _showFigureInfo);
        break;
      case FigureBaseType.Knight:
        return _buildCardsList(
            context: context,
            enumValues: KnightId.values,
            imageGetter: DeckDrawer.getKnightAvatarWtf,
            textGetter: FigureLocalizations.of(context).getKnightName,
            selector: _selectKnightFigure,
            shower: _showFigureInfo);
        break;
      case FigureBaseType.Pawn:
        return _buildCardsList(
            context: context,
            enumValues: PawnId.values,
            imageGetter: DeckDrawer.getPawnAvatarWtf,
            textGetter: FigureLocalizations.of(context).getPawnName,
            selector: _selectPawnFigure,
            shower: _showFigureInfo);
        break;
      case FigureBaseType.Queen:
        return _buildCardsList(
            context: context,
            enumValues: QueenId.values,
            imageGetter: DeckDrawer.getQueenAvatarWtf,
            textGetter: FigureLocalizations.of(context).getQueenName,
            selector: _selectQueenFigure,
            shower: _showFigureInfo);
        break;
      case FigureBaseType.Rock:
        return _buildCardsList(
            context: context,
            enumValues: RockId.values,
            imageGetter: DeckDrawer.getRockAvatarWtf,
            textGetter: FigureLocalizations.of(context).getRockName,
            selector: _selectRockFigure,
            shower: _showFigureInfo);
        break;
    }
  }

  List<Widget> _buildCardsList({
    @required BuildContext context,
    @required var enumValues,
    @required Widget imageGetter(var id),
    @required String textGetter(var id),
    @required VoidCallback selector(BuildContext context, var id),
    @required VoidCallback shower(BuildContext context, var id)})
  {
    double h = MediaQuery.of(context).size.height;

    double buttonH = h * 0.15;
    return [
      for(var id in enumValues)
        Container(
          width: (h - buttonH) * 0.75,
          child: Column(
            children: <Widget>[
              GestureDetector(
                onTap: selector(context, id),
                onLongPress: () {setState(shower(context, id));},
                child: DeckDrawer.drawCard(
                  image: imageGetter(id),
                  h: h - buttonH,
                  name: textGetter(id),
                ),
              ),
              Row(
                children: <Widget>[
                  Spacer(),
                  Expanded(
                    child: SimpleBoxButton(
                      height: buttonH,
                      minWidth: buttonH,
                      icon: Icon(
                        Icons.check,
                        color: CustomColors.LightA,
                      ),
                      onTap: selector(context, id),
                    ),
                    flex: 12,
                  ),
                  Spacer(),
                  Expanded(
                    child: SimpleBoxButton(
                      height: buttonH,
                      minWidth: buttonH,
                      icon: Icon(
                        Icons.info_outline,
                        color: CustomColors.LightA,
                      ),
                      onTap: () {setState(shower(context, id));},
                    ),
                    flex: 12,
                  ),
                  Spacer(),
                ],
              ),
            ],
          ),
        ),
    ];
  }

  VoidCallback _selectBishopFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.bishops[_selectedCard] = id;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectPawnFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.pawns[_selectedCard] = id;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectKingFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.kings[_selectedCard] = id;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectQueenFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.queens[_selectedCard] = id;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectRockFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.rocks[_selectedCard] = id;
      Navigator.pop(context);
    };
  }
  VoidCallback _selectKnightFigure(BuildContext context, dynamic id) {
    return () {
      _selectedDeck.knights[_selectedCard] = id;
      Navigator.pop(context);
    };
  }






  VoidCallback _showFigureInfo(BuildContext context, dynamic id) {
    return () {
      figureSubTupe = id;
      showInfo = true;
    };
  }



  Widget _drawFigureInfo({
    @required BuildContext context,
  }) {
    FigureBase figure = ProviderAllDecks.getFigureClass(figureSubTupe);

    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    double iconSize = h / 16;
    double abilityIconSize = h / 7;
    double buttonH = h * 0.15;
    double fontSize = h * 0.05;

    return Container(
      color: Color.fromRGBO(
          CustomColors.DarkA.red,
          CustomColors.DarkA.green,
          CustomColors.DarkA.blue,
          120
      ),
      height: h,
      width: w,
      alignment: Alignment.center,
      child: Container(
        height: h * 0.8,
        width: w * 0.9,
        decoration: BoxDecoration(
          color: Color.fromRGBO(
              CustomColors.Main1A.red,
              CustomColors.Main1A.green,
              CustomColors.Main1A.blue,
              20
          ),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          children: <Widget>[
            Container(
              height: h * 0.8,
              width: w * 0.45,
              child: Column(
                children: <Widget>[
                  Spacer(),
                  Row(
                    children: <Widget>[
                      Container(
                        height: h / 2,
                        width: h / 2,
                        child: DeckDrawer.getFigureAvatarWtf(figureSubTupe),
                      ),
                      Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  for(int h = 0; h < figure.maxHPAndArmor; ++h)
                                    Image.asset(
                                      "assets/icons/hp/health_point_back.png",
                                      height: iconSize,
                                      width: iconSize,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  for(int h = 0; h < figure.health; ++h)
                                    Image.asset(
                                      "assets/icons/hp/health_point.png",
                                      height: iconSize,
                                      width: iconSize,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                            ],
                          ),
                          Stack(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  for(int h = 0; h < figure.maxHPAndArmor; ++h)
                                    Image.asset(
                                      "assets/icons/hp/armor_back.png",
                                      height: iconSize,
                                      width: iconSize,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  for(int h = 0; h < figure.armor; ++h)
                                    Image.asset(
                                      "assets/icons/hp/armor.png",
                                      height: iconSize,
                                      width: iconSize,
                                      fit: BoxFit.cover,
                                    ),
                                ],
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/icons/damage/atack.png",
                                height: iconSize,
                                width: iconSize,
                                fit: BoxFit.cover,
                              ),
                              SimpleText(
                                color: CustomColors.LightA,
                                size: iconSize,
                                text: figure.atackDamage.toString(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  Spacer(),
                  Row(
                    children: <Widget>[
                      Spacer(),
                      Container(
                        width: abilityIconSize,
                        height: abilityIconSize,
                        child: Image.asset("assets/game/yellow.png"),
                      ),

                      Container(
                        width: abilityIconSize,
                        height: abilityIconSize,
                        child: Image.asset("assets/game/yellow.png"),
                      ),

                      Container(
                        width: abilityIconSize,
                        height: abilityIconSize,
                        child: Image.asset("assets/game/yellow.png"),
                      ),
                      Spacer(),
                    ],
                  ),
                  Spacer(),
                ],
              ),
            ),
            Container(
              height: h * 0.8,
              width: w * 0.45,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Spacer(),
                      SimpleBoxButton(
                        height: buttonH,
                        minWidth: buttonH,
                        icon: Icon(Icons.close, color: CustomColors.LightA,),
                        onTap: () {
                          setState(() {
                            showInfo = false;
                          });
                        },
                      ),
                    ],
                  ),
                  Wrap(
                    children: <Widget>[
                      SimpleText(
                        text: FigureLocalizations.of(context).getFigureDesc(figureSubTupe),
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ],
                  ),
                  Spacer(),
                ],
              )
            ),
          ],
        )
      ),
    );
  }

}



VoidCallback _procPrevSave(BuildContext context, int selectedIdx, Deck newDeck) {
  return () {
    ProviderAllDecks.getInst().decks[selectedIdx] = newDeck;
    ProviderAllDecks.saveToFile();
    Navigator.pop(context);
  };
}


VoidCallback _procPrevDiscard(BuildContext context) {
  return () {
    Navigator.pop(context);
  };
}