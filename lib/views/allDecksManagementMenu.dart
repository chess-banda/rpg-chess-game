
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rpgchessgame/models/Deck.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';
import '../models/AllDecks.dart';
import '../models/Player.dart';
import 'package:rpgchessgame/CustomColors.dart';
import 'package:rpgchessgame/CustomWidgets.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/views/deckManagementMenu.dart';

import '../Localization.dart';
import '../main.dart';

enum Delete {
  Yes,
  No
}


class AllDecksManagementMenu extends StatefulWidget{
  final bool selectMode;
  final int playerIndex;
  final List<Player> players;
  AllDecksManagementMenu({
    this.selectMode = false,
    this.playerIndex,
    this.players
  });

  @override
  State<StatefulWidget> createState() =>
      _AllDecksManagementMenuState(selectMode, playerIndex, players);
}

class _AllDecksManagementMenuState extends State<AllDecksManagementMenu> {
  final bool _selectMode;
  int playerIndex;
  int selectedIdx = 0;
  List<Player> players;


  _AllDecksManagementMenuState(
      this._selectMode,
      this.playerIndex,
      this.players
      );

  @override
  Widget build(BuildContext context) {
    double h = MediaQuery.of(context).size.height;
    double w = MediaQuery.of(context).size.width;
    double buttonH = h * 0.15;

    return Scaffold(
        backgroundColor: CustomColors.DarkA,
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: w / 2,
              child: Column(
                children: <Widget>[
                  Container(
                    height: h - buttonH,
                    width: h - buttonH,
                    child:  (ProviderAllDecks.getInst().decks.length == 0
                        ? null
                        : _drawDeckGrid(ProviderAllDecks
                        .getInst()
                        .decks[selectedIdx = (
                        ProviderAllDecks.getInst().decks.length <= selectedIdx
                            ? 0
                            : selectedIdx)])
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[

//                        Padding(
//                          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
//                          child: SimpleBoxButton(
//                            onTap: _procSave(context),
//                            icon: Icon(
//                              Icons.save,
//                              color: CustomColors.LightA,
//                            ),
//                            minWidth: buttonH,
//                            height: buttonH,
//                          ),
//                        ),
                        SimpleBoxButton(
                          onTap: _procPrev(context),
                          icon: Icon(Icons.arrow_back, color: CustomColors.LightA,),
                          minWidth: buttonH,
                          height: buttonH,
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: w / 2,
              child: ListView(
                children: <Widget>[
                  for(int i = 0; i < ProviderAllDecks.getInst().decks.length; ++i)
                    DeckEntry(
                      selected: i == selectedIdx,
                      selectMode: _selectMode,
                      text: ProviderAllDecks.getInst().decks[i].name,
                      idx: i,
                      flatButtonCallback: () {
                        selectedIdx = i;
                        setState(() {});
                      },
                      editCallback: _selectMode
                          ? _procSelect(context, i)
                          : _procEdit(context, i),
                      deleteCallback: () async {
                        await _procDelete(context, i);
                        setState(() {});
                      }
                    ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: SimpleBoxButton(
                            onTap: () {
                              ProviderAllDecks.getInst().addDefaultDeck();
                              setState(() {});
                            },
                            icon: Icon(Icons.add, color: CustomColors.LightA),
                            minWidth: buttonH,
                            height: buttonH,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
    );
  }

  Future<void> _procDelete(BuildContext context, int deleteIdx) async {
    final double fontSize = MediaQuery.of(context).size.height * 0.06;
    switch(await showDialog<Delete>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: CustomColors.Main1C,
            title: Center(
              child: SimpleText(
                text: GLocalizations.of(context).allDecksManagementMenuDeleteDialogTitle,
                color: CustomColors.LightA, size: fontSize,
              ),
            ),
            children: <Widget>[
              Row(
                children: <Widget>[
                  Spacer(flex: 1,),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, Delete.Yes);
                    },
                    child: SimpleText(
                      text: GLocalizations.of(context).allDecksManagementMenuDeleteDialogYes,
                      color: CustomColors.LightA, size: fontSize,
                    ),
                  ),
                  Spacer(flex: 1,),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, Delete.No);
                    },
                    child: SimpleText(
                      text: GLocalizations.of(context).allDecksManagementMenuDeleteDialogNo,
                      color: CustomColors.LightA, size: fontSize,
                    ),
                  ),
                  Spacer(flex: 1,),
                ],
              ),
            ],
          );
        })) {
      case Delete.Yes:
        {
          ProviderAllDecks.getInst().removeDeck(deleteIdx);

        }
        break;
      case Delete.No: {} break;
    }
  }

  VoidCallback _procSave(BuildContext context) {
    return () {
//      double h = MediaQuery.of(context).size.height;
//      ProviderAllDecks.saveToFile();
//      showDialog(
//        context: context,
//        builder: (_context) => AlertDialog(
//          backgroundColor: CustomColors.Main1C,
//          title: Center(
//            child: SimpleText(
//              color: CustomColors.LightA,
//              size: h * 0.06,
//              text: GLocalizations.of(context).allDecksManagementMenuSaveText,
//            ),
//          ),
//          actions: <Widget>[
//            SimpleBoxButton(
//              height: h * 0.15,
//              minWidth: h * 0.15,
//              icon: Icon(Icons.check, color: CustomColors.LightA,),
//              onTap: () {
//                Navigator.pop(_context);
//              },
//            ),
//
//          ],
//        )
//      );
    };
  }

  VoidCallback _procPrev(BuildContext context) {
    return () {
      ProviderAllDecks.saveToFile();
      Navigator.pop(context);
    };
  }

  VoidCallback _procEdit(BuildContext context, int selectedIdx) {
    return () {
      Navigator.push(
          context, MaterialPageRoute(
        builder: (context) => DeckManagementMenu(selectedIdx),
      )
      );
    };
  }

  VoidCallback _procSelect(BuildContext context, int selectedIdx) {
    return () {
      players[playerIndex].selectedDeckIdx = selectedIdx;
      Navigator.pop(context);
    };
  }


  Widget _drawDeckGrid(Deck deck) {
    return GridView.count(
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 4,
      padding: EdgeInsets.all(3),
      mainAxisSpacing: 3,
      crossAxisSpacing: 3,
      children: <Widget>[
        for(int i = 0; i < 16; ++i)
          Stack(
            children: <Widget>[
              if(i < 8) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].pawns[i]),
              if(i == 8) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].rocks[0]),
              if(i == 9) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].knights[0]),
              if(i == 10) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].bishops[0]),
              if(i == 11) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].kings[0]),
              if(i == 12) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].queens[0]),
              if(i == 13) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].bishops[1]),
              if(i == 14) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].knights[1]),
              if(i == 15) DeckDrawer.getFigureAvatar(ProviderAllDecks.getInst().decks[selectedIdx].rocks[1]),

              GestureDetector(
                onTap: () {
                  var builder;
                  if(i < 8) builder = (context) => CardSelector.pawn(i, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 8) builder = (context) => CardSelector.rock(0, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 9) builder = (context) => CardSelector.knight(0, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 10) builder = (context) => CardSelector.bishop(0, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 11) builder = (context) => CardSelector.king(0, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 12) builder = (context) => CardSelector.queen(0, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 13) builder = (context) => CardSelector.bishop(1, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 14) builder = (context) => CardSelector.knight(1, ProviderAllDecks.getInst().decks[selectedIdx]);
                  else if(i == 15) builder = (context) => CardSelector.rock(1, ProviderAllDecks.getInst().decks[selectedIdx]);
                  Navigator.push(
                  context, MaterialPageRoute(
                  builder: builder,
                  )
                  );
                },
              ),
            ],
          ),

      ],
    );
  }
}


class DeckEntry extends StatelessWidget{

  final bool selected;
  final String text;
  final int idx;
  final VoidCallback flatButtonCallback;
  final VoidCallback editCallback;
  final VoidCallback deleteCallback;
  final bool selectMode;
  const DeckEntry({
    @required this.selected,
    @required this.text,
    @required this.idx,
    @required this.flatButtonCallback,
    @required this.editCallback,
    @required this.deleteCallback,
    @required this.selectMode,
  });

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    double buttonH = h * 0.15;
    double buttonW = w / 2 - buttonH * 3;
    double textSize = h * 0.05;
    Color buttonColor = selected ? CustomColors.Secondary2D : CustomColors.Main1C;


    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SimpleButton(
            child: FlatButton(
              onPressed: flatButtonCallback,
              color: buttonColor,
              child: SimpleText(
                text: text,
                color: CustomColors.LightA,
                size: textSize,
              ),
            ),
            minWidth: buttonW,
            height: buttonH,
          ),
          Spacer(flex: 1,),
          SimpleBoxButton(
            onTap: editCallback,
            icon: selectMode
                ? Icon(Icons.check, color: CustomColors.LightA,)
                : Icon(Icons.edit, color: CustomColors.LightA,),
            minWidth: buttonH, height: buttonH,
          ),
          Spacer(flex: 1,),
          SimpleBoxButton(
            onTap: deleteCallback,
            icon: Icon(Icons.delete, color: CustomColors.LightA,),
            minWidth: buttonH, height: buttonH,
          ),
        ],
      ),
    );
  }
}



