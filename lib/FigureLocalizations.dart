import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';


class FigureLocalizations {
  FigureLocalizations(this.locale);

  final Locale locale;

  static FigureLocalizations of(BuildContext context) {
    return Localizations.of<FigureLocalizations>(context, FigureLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'warriorName' : 'Warrior',
      'warriorDesc' : '''A loyal soldier serving the crown. Every warrior is trained in the art of handling the sword so that sometimes it seems as if his sword cuts through opponents. His skill is able to inspire others, increasing their attack.''',

      'mageName' : 'Mage',
      'mageDesc' : '''    Graduate of the College of magicians. With basic knowledge in the field of destruction magic, it is able to attack enemies with a variety of magic, as well as curse the most difficult opponents.''',

      'casterName' : 'Caster',
      'casterDesc' : '''    The wisest of magicians, his mind is able to shut everyone up. With the power of secret magic, the king is able to buy the necessary time to win.''',

      'palladinName' : 'Palladin',
      'palladinDesc' : '''    The king must protect his subjects. This is the principle that guides him. Not just protection-the paladin is able to return his ally literally from the other world.''',

      'strategistName' : 'Strategist',
      'strategistDesc' : '''    Flexible mind and tactical thinking. He does not know how to fight at all, but using his strengths, he can anticipate the next move of his enemy and beat him. Following his strategy, victory will not be long in coming.''',

      'riderName' : 'Rider',
      'riderDesc' : '''   A typical representative of mounted troops. Skillfully handling a horse, it is able to maneuver effectively on the battlefield, and literally trample all the enemies in its path.''',

      'archerName' : 'Archer',
      'archerDesc' : '''    A hard-working conscript fighting for his king. Does not have special abilities, because only yesterday the chief issued a sword and sent it into battle.''',

      'swordsMenName' : 'SwordsMen',
      'swordsMenDesc' : '''    Just as hard-working as a swordsman. Has no special abilities, but, unlike his colleague, managed to beat out a place in the rear and bought a bow.''',

      'archMageName' : 'ArchMage',
      'archMageDesc' : '''    The most powerful mage in the obedience of the king. Having learned most of the magic techniques, he is able to summon a firestorm in the ranks of the enemy. Not only destruction, but restoration is subject to the Archmage – in a moment he can restore the strength of the allies.''',

      'assassinName' : 'Assassin',
      'assassinDesc' : '''    Not everyone likes to fight in the open. Using traps and hidden attacks, the killer is able to destroy their enemies without being exposed to the blow.''',

      'championName' : 'Champion',
      'championDesc' : '''    The best warrior in the king's service. No one can escape the blow of his sword. On the battlefield, he inspires all his closest allies with his skill.''',

      'berserkName' : 'Berserk',
      'berserkDesc' : '''   A skilled warrior who lives by fighting. In the heat of battle, he goes into a rage, destroying everything in his path. A real death machine for both your enemies and yourself.''',

      'shielderName' : 'Shielder',
      'shielderDesc' : '''    Just look at this tin can! The most armored soldier on the battlefield. Following its purpose, this heavyweight protects allies on the battlefield. However, if someone suddenly decides to "open" it, the shield (defender) can crush unlucky enemies with its armor.''',
    },
    'ru': {
      'warriorName' : 'Воин',
      'warriorDesc' : '''   Верный солдат, служащий короне. Каждый воин обучен искусству обращения с мечом настолько, что порой, кажется, будто его меч разит противников насквозь. Своим мастерством способен воодушевлять других, увеличивая их атаку.''',

      'mageName' : 'Маг',
      'mageDesc' : '''    Выпускник коллегии магов. Обладая начальными знаниями в области магии разрушения, способен атаковать врагов разнообразной магией, а также проклинать наиболее затруднительных противников.''',

      'casterName' : 'Заклинатель',
      'casterDesc' : '''    Мудрейший из магов, своим умом способен заткнуть всех и каждого. Обладая силой тайной магии, король способен выиграть необходимое для победы время.''',

      'palladinName' : 'Палладин',
      'palladinDesc' : '''    Король обязан защищать своих подданых. Именно таким принципом он руководствуется. Не просто защита – паладин способен вернуть своего союзника буквально с того света.''',

      'strategistName' : 'Стратег',
      'strategistDesc' : '''    Гибкий ум и тактическое мышление. Он совершенно не умеет сражаться, но, используя свои сильные стороны, может предугадать следующий ход своего врага и обыграть его. Следуя его стратегии, победа не заставит себя долго ждать.''',

      'riderName' : 'Наездник',
      'riderDesc' : '''   Типичный представитель конных войск. Умело обращаясь с лошадью, он способен как эффективно маневрировать на поле боя, так и буквально затоптать всех врагов на своем пути.''',

      'archerName' : 'Лучник',
      'archerDesc' : '''    Такой же работяга, как и мечник. Не имеет особых способностей, но, в отличие от своего коллеги, сумел выбить себе место в тылу и купил лук.''',

      'swordsMenName' : 'Мечник',
      'swordsMenDesc' : '''   Работяга – призывник, сражающийся за своего короля. Не имеет особых способностей, ибо только вчера начальник выдал меч и отправил в бой.''',

      'archMageName' : 'Архимаг',
      'archMageDesc' : '''    Наиболее могущественный маг в подчинении короля. Изучив большинство магических техник, он способен призвать огненный шторм в рядах противника. Не только разрушение, но восстановление подвластно архимагу – вмиг может восстановить он силы союзников.''',

      'assassinName' : 'Убийца',
      'assassinDesc' : '''    Не все любят сражаться в открытую. Использую ловушки и скрытые атаки, убийца способен уничтожать своих врагов, не подставляясь под удар.''',

      'championName' : 'Чемпион',
      'championDesc' : '''    Лучший воин на службе у короля. Никто не может спастись от удара его меча. На поле боя своим мастерством он воодушевляет всех ближайших союзников.''',

      'berserkName' : 'Берсерк',
      'berserkDesc' : '''   Умелый воин, живущий сражениями. В пылу битвы он впадает в ярость, круша все на своем пути. Настоящая машина смерти как для врагов, так и для себя самого.''',

      'shielderName' : 'Защитник',
      'shielderDesc' : '''    Только взгляните на эту консервную банку! Самый бронированный солдат на поле боя. Следуя своему предназначению, этот тяжеловес защищает союзников на поле боя. Однако, если кто-то вдруг решит его “вскрыть”, щитовик (защитник) может раздавить невезучих врагов своей броней.''',
    },
  };

  String getPawnName(dynamic id) {
    switch(id) {
      case PawnId.Archer: return archerName;
      case PawnId.SwordsMen: return swordsMenName;
    }
  }

  String getBishopName(dynamic id) {
    switch(id) {
      case BishopId.Mage: return mageName;
      case BishopId.Warrior: return warriorName;
    }
  }

  String getKingName(dynamic id) {
    switch(id) {
      case KingId.Strategist: return strategistName;
      case KingId.Caster: return casterName;
      case KingId.Palladin: return palladinName;
    }
  }

  String getKnightName(dynamic id) {
    switch(id) {
      case KnightId.Rider: return riderName;
    }
  }

  String getQueenName(dynamic id) {
    switch(id) {
      case QueenId.Assassin: return assassinName;
      case QueenId.ArchMage: return archMageName;
      case QueenId.Champion: return championName;
    }
  }

  String getRockName(dynamic id) {
    switch(id) {
      case RockId.Berserk: return berserkName;
      case RockId.Shielder: return shielderName;
    }
  }


  String getFigureName(dynamic id) {
    if(id is BishopId) return getBishopName(id);
    else if(id is KingId) return getKingName(id);
    else if(id is QueenId) return getQueenName(id);
    else if(id is KnightId) return getKnightName(id);
    else if(id is RockId) return getRockName(id);
    else if(id is PawnId) return getPawnName(id);
  }

  String getFigureDesc(dynamic id) {
    if(id is BishopId) return getBishopDesc(id);
    else if(id is KingId) return getKingDesc(id);
    else if(id is QueenId) return getQueenDesc(id);
    else if(id is KnightId) return getKnightDesc(id);
    else if(id is RockId) return getRockDesc(id);
    else if(id is PawnId) return getPawnDesc(id);
  }


  String getPawnDesc(dynamic id) {
    switch(id) {
      case PawnId.Archer: return archerDesc;
      case PawnId.SwordsMen: return swordsMenDesc;
    }
  }

  String getBishopDesc(dynamic id) {
    switch(id) {
      case BishopId.Mage: return mageDesc;
      case BishopId.Warrior: return warriorDesc;
    }
  }

  String getKingDesc(dynamic id) {
    switch(id) {
      case KingId.Strategist: return strategistDesc;
      case KingId.Caster: return casterDesc;
      case KingId.Palladin: return palladinDesc;
    }
  }

  String getKnightDesc(dynamic id) {
    switch(id) {
      case KnightId.Rider: return riderDesc;
    }
  }

  String getQueenDesc(dynamic id) {
    switch(id) {
      case QueenId.Assassin: return assassinDesc;
      case QueenId.ArchMage: return archMageDesc;
      case QueenId.Champion: return championDesc;
    }
  }

  String getRockDesc(dynamic id) {
    switch(id) {
      case RockId.Berserk: return berserkDesc;
      case RockId.Shielder: return shielderDesc;
    }
  }


  String get warriorName => _localizedValues[locale.languageCode]['warriorName'];
  String get warriorDesc => _localizedValues[locale.languageCode]['warriorDesc'];
  String get mageName => _localizedValues[locale.languageCode]['mageName'];
  String get mageDesc => _localizedValues[locale.languageCode]['mageDesc'];
  String get casterName => _localizedValues[locale.languageCode]['casterName'];
  String get casterDesc => _localizedValues[locale.languageCode]['casterDesc'];
  String get palladinName => _localizedValues[locale.languageCode]['palladinName'];
  String get palladinDesc => _localizedValues[locale.languageCode]['palladinDesc'];
  String get strategistName => _localizedValues[locale.languageCode]['strategistName'];
  String get strategistDesc => _localizedValues[locale.languageCode]['strategistDesc'];
  String get riderName => _localizedValues[locale.languageCode]['riderName'];
  String get riderDesc => _localizedValues[locale.languageCode]['riderDesc'];
  String get archerName => _localizedValues[locale.languageCode]['archerName'];
  String get archerDesc => _localizedValues[locale.languageCode]['archerDesc'];
  String get swordsMenName => _localizedValues[locale.languageCode]['swordsMenName'];
  String get swordsMenDesc => _localizedValues[locale.languageCode]['swordsMenDesc'];
  String get archMageName => _localizedValues[locale.languageCode]['archMageName'];
  String get archMageDesc => _localizedValues[locale.languageCode]['archMageDesc'];
  String get assassinName => _localizedValues[locale.languageCode]['assassinName'];
  String get assassinDesc => _localizedValues[locale.languageCode]['assassinDesc'];
  String get championName => _localizedValues[locale.languageCode]['championName'];
  String get championDesc => _localizedValues[locale.languageCode]['championDesc'];
  String get berserkName => _localizedValues[locale.languageCode]['berserkName'];
  String get berserkDesc => _localizedValues[locale.languageCode]['berserkDesc'];
  String get shielderName => _localizedValues[locale.languageCode]['shielderName'];
  String get shielderDesc => _localizedValues[locale.languageCode]['shielderDesc'];

}
class FigureLocalizationsDelegate extends LocalizationsDelegate<FigureLocalizations> {
  const FigureLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<FigureLocalizations> load(Locale locale) {
    return SynchronousFuture<FigureLocalizations>(FigureLocalizations(locale));
  }

  @override
  bool shouldReload(FigureLocalizationsDelegate old) => false;
}
