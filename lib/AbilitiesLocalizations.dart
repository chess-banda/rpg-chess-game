import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';


class AbilitiesLocalizations {
  AbilitiesLocalizations(this.locale);

  final Locale locale;

  static AbilitiesLocalizations of(BuildContext context) {
    return Localizations.of<AbilitiesLocalizations>(context, AbilitiesLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      '' : '',
      '' : '',
      '' : '',
    },
    'ru': {
      '' : '',
      '' : '',
      '' : '',
    },
  };

//String get  => _localizedValues[locale.languageCode][''];
//String get  => _localizedValues[locale.languageCode][''];
//String get  => _localizedValues[locale.languageCode][''];

}
class AbilitiesLocalizationsDelegate extends LocalizationsDelegate<AbilitiesLocalizations> {
  const AbilitiesLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<AbilitiesLocalizations> load(Locale locale) {
    return SynchronousFuture<AbilitiesLocalizations>(AbilitiesLocalizations(locale));
  }

  @override
  bool shouldReload(AbilitiesLocalizationsDelegate old) => false;
}
