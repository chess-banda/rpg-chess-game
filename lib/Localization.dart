import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:rpgchessgame/AbilitiesLocalizations.dart';
import 'package:rpgchessgame/FigureLocalizations.dart';

class GLocalizations {
  GLocalizations(this.locale);

  final Locale locale;

  static GLocalizations of(BuildContext context) {
    return Localizations.of<GLocalizations>(context, GLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'mainMenuButtonStartGame': 'Start game',
      'mainMenuButtonHelp' : 'Help',
      'mainMenuButtonDeckManagement' : 'Managing decks',
      'mainMenuTextNameOfGame' : 'RPChessGame',
      'mainMenuTextExitDialogTitle' : 'Leave the game?',
      'mainMenuTextExitDialogYes' : 'Yes',
      'mainMenuTextExitDialogNo' : 'No',


      'startGameMenuTextPlayersCount' : 'Number of players ',
      'startGameMenuTextTimeCount' : 'Game time ',
      'startGameMenuTextTimeMinutes' : ' minutes',
      'startGameMenuTextChooseTeamColor' : 'The team colors (marked with a flag) must not be repeated.',
      'startGameMenuTextChooseDecks' : 'You didn\'t choose the decks.',
      'startGameMenuButtonChangeBackground' : 'Change background',
      'startGameMenuButtonStartGame' : 'Start',
      'startGameMenuButtonConfigPlayers' : 'To configure the players',


      'allDecksManagementMenuDeleteDialogTitle' : 'Really delete a deck?',
      'allDecksManagementMenuDeleteDialogYes' : 'Yes',
      'allDecksManagementMenuDeleteDialogNo' : 'No',
      'allDecksManagementMenuSaveText' : 'Saved',


      'deckManagementMenuTextEditName' : 'Name',

      'playersEditorButtonSelectDeck' : 'Select deck',


      'gameAbilityText' : 'Abilities',
      'gameTimerText' : 'Remaining time: ',
      'gameKingDestroyedText' : 'The enemy\'s king is destroyed',
      'gameTimeLimitExceededText' : 'The enemy has run out of time',
      'gameAllFiguresDestroyedText' : 'All pieces of the opponent are destroyed',
      'gameWinText' : 'Won: ',
      '' : '',
      '' : '',
    },
    'ru': {
      'mainMenuButtonStartGame': 'Начать игру',
      'mainMenuButtonHelp' : 'Помощь',
      'mainMenuButtonDeckManagement' : 'Управление колодами',
      'mainMenuTextNameOfGame' : 'RPChessGame',
      'mainMenuTextExitDialogTitle' : 'Покинуть игру?',
      'mainMenuTextExitDialogYes' : 'Да',
      'mainMenuTextExitDialogNo' : 'Нет',


      'startGameMenuTextPlayersCount' : 'Количество игроков ',
      'startGameMenuTextTimeCount' : 'Время игры ',
      'startGameMenuTextTimeMinutes' : ' минут',
      'startGameMenuTextChooseTeamColor' : 'Цвета команд (обозначены флажком) не должны повторяться.',
      'startGameMenuTextChooseDecks' : 'Вы не выбрали колоды.',
      'startGameMenuButtonChangeBackground' : 'Изменить фон',
      'startGameMenuButtonStartGame' : 'Начать',
      'startGameMenuButtonConfigPlayers' : 'Настроить игроков',


      'allDecksManagementMenuDeleteDialogTitle' : 'Удалить колоду?',
      'allDecksManagementMenuDeleteDialogYes' : 'Да',
      'allDecksManagementMenuDeleteDialogNo' : 'Нет',
      'allDecksManagementMenuSaveText' : 'Сохранено',


      'deckManagementMenuTextEditName' : 'Название',

      'playersEditorButtonSelectDeck' : 'Выбрать колоду',


      'gameAbilityText' : 'Способности',
      'gameTimerText' : 'Оставшееся время: ',
      'gameKingDestroyedText' : 'Король противника уничтожен',
      'gameTimeLimitExceededText' : 'У противника закончилось время',
      'gameAllFiguresDestroyedText' : 'Все фигуры противника уничтожены',
      'gameWinText' : 'Победили: ',
      '' : '',
      '' : '',

    },
  };

  String get mainMenuButtonStartGame => _localizedValues[locale.languageCode]['mainMenuButtonStartGame'];
  String get mainMenuButtonHelp => _localizedValues[locale.languageCode]['mainMenuButtonHelp'];
  String get mainMenuButtonDeckManagement => _localizedValues[locale.languageCode]['mainMenuButtonDeckManagement'];
  String get mainMenuTextNameOfGame => _localizedValues[locale.languageCode]['mainMenuTextNameOfGame'];
  String get mainMenuTextExitDialogTitle => _localizedValues[locale.languageCode]['mainMenuTextExitDialogTitle'];
  String get mainMenuTextExitDialogYes => _localizedValues[locale.languageCode]['mainMenuTextExitDialogYes'];
  String get mainMenuTextExitDialogNo => _localizedValues[locale.languageCode]['mainMenuTextExitDialogNo'];
  String get startGameMenuTextPlayersCount => _localizedValues[locale.languageCode]['startGameMenuTextPlayersCount'];
  String get startGameMenuTextTimeCount => _localizedValues[locale.languageCode]['startGameMenuTextTimeCount'];
  String get startGameMenuTextTimeMinutes => _localizedValues[locale.languageCode]['startGameMenuTextTimeMinutes'];
  String get startGameMenuTextChooseTeamColor => _localizedValues[locale.languageCode]['startGameMenuTextChooseTeamColor'];
  String get startGameMenuTextChooseDecks => _localizedValues[locale.languageCode]['startGameMenuTextChooseDecks'];
  String get startGameMenuButtonChangeBackground => _localizedValues[locale.languageCode]['startGameMenuButtonChangeBackground'];
  String get startGameMenuButtonStartGame => _localizedValues[locale.languageCode]['startGameMenuButtonStartGame'];
  String get startGameMenuButtonConfigPlayers => _localizedValues[locale.languageCode]['startGameMenuButtonConfigPlayers'];
  String get allDecksManagementMenuDeleteDialogTitle => _localizedValues[locale.languageCode]['allDecksManagementMenuDeleteDialogTitle'];
  String get allDecksManagementMenuDeleteDialogYes => _localizedValues[locale.languageCode]['allDecksManagementMenuDeleteDialogYes'];
  String get allDecksManagementMenuDeleteDialogNo => _localizedValues[locale.languageCode]['allDecksManagementMenuDeleteDialogNo'];
  String get deckManagementMenuTextEditName => _localizedValues[locale.languageCode]['deckManagementMenuTextEditName'];
  String get playersEditorButtonSelectDeck => _localizedValues[locale.languageCode]['playersEditorButtonSelectDeck'];
  String get gameAbilityText => _localizedValues[locale.languageCode]['gameAbilityText'];
  String get gameTimerText => _localizedValues[locale.languageCode]['gameTimerText'];
  String get gameKingDestroyedText => _localizedValues[locale.languageCode]['gameKingDestroyedText'];
  String get gameTimeLimitExceededText => _localizedValues[locale.languageCode]['gameTimeLimitExceededText'];
  String get gameAllFiguresDestroyedText => _localizedValues[locale.languageCode]['gameAllFiguresDestroyedText'];
  String get gameWinText => _localizedValues[locale.languageCode]['gameWinText'];
  String get allDecksManagementMenuSaveText => _localizedValues[locale.languageCode]['allDecksManagementMenuSaveText'];
  //String get  => _localizedValues[locale.languageCode][''];
  //String get  => _localizedValues[locale.languageCode][''];
  //String get  => _localizedValues[locale.languageCode][''];
  //String get  => _localizedValues[locale.languageCode][''];

}
class GLocalizationsDelegate extends LocalizationsDelegate<GLocalizations> {
  const GLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ru'].contains(locale.languageCode);

  @override
  Future<GLocalizations> load(Locale locale) {
    return SynchronousFuture<GLocalizations>(GLocalizations(locale));
  }

  @override
  bool shouldReload(GLocalizationsDelegate old) => false;
}





