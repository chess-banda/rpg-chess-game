import 'package:flutter/material.dart';
import 'package:rpgchessgame/CustomColors.dart';

class SimpleText extends StatelessWidget{
  final String text;
  final double size;
  final Color color;
  const SimpleText({@required this.text, @required this.size, @required this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: size),
    );
  }
}

class FigureNameText extends StatelessWidget{
  final String text;
  final double size;
  final Color color;
  const FigureNameText({@required this.text, @required this.size, @required this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: size),
    );
  }
}

class SimpleButton extends StatelessWidget{
  final Widget child;
  final double minWidth;
  final double height;
  const SimpleButton({@required this.child, @required this.minWidth, @required this.height});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(

      buttonColor: CustomColors.Main1C,
      focusColor: CustomColors.Main1C,
      splashColor: CustomColors.Main1B,
      hoverColor: CustomColors.Main1C,
      disabledColor: CustomColors.Main1C,
      highlightColor: CustomColors.Main1C,
      child: child,
      minWidth: minWidth,
      height: height,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
    );
  }
}

class SimpleBoxButton extends StatelessWidget {
  final VoidCallback onTap;
  final Widget icon;
  final double minWidth;
  final double height;
  const SimpleBoxButton({
  @required this.onTap,
  @required this.icon,
  @required this.minWidth,
  @required this.height
  });

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      buttonColor: CustomColors.Main1C,
      focusColor: CustomColors.Main1C,
      splashColor: CustomColors.Main1B,
      hoverColor: CustomColors.Main1C,
      disabledColor: CustomColors.Main1C,
      highlightColor: CustomColors.Main1C,
      child: FlatButton(
        child: icon,
        onPressed: onTap,
        color: CustomColors.Main1C,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      minWidth: minWidth,
      height: height,
    );
  }
}