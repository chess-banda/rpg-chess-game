import 'package:flutter/material.dart';

class CustomColors {
  //MAIN1
  static const Color Main1A = Color(0xFF40045A);
  static const Color Main1B = Color(0xFF650049);
  static const Color Main1C = Color(0xFF2A085C);

  //MAIN2
  static const Color Main2A = Color(0xFF40045A);
  static const Color Main2B = Color(0xFF351344);
  static const Color Main2C = Color(0xFF2A013B);
  static const Color Main2D = Color(0xFF8B34B1);
  static const Color Main2E = Color(0xFF9657B1);

  //DARK
  static const Color DarkA = Color(0xFF383B40);
  static const Color DarkB = Color(0xFF2D2F34);
  static const Color DarkC = Color(0xFF27292D);
  static const Color DarkD = Color(0xFF1F2023);
  static const Color DarkE = Color(0xFF010101);

  //LIGHT
  static const Color LightA = Color(0xFFFFFFFF);
  static const Color LightB = Color(0xB2FFFFFF);
  static const Color LightC = Color(0x7FFFFFFF);

  //SECONDARY1
  static const Color Secondary1A = Color(0xFF650049);
  static const Color Secondary1B = Color(0xFF4C133C);
  static const Color Secondary1C = Color(0xFF420030);
  static const Color Secondary1D = Color(0xFFB63091);
  static const Color Secondary1E = Color(0xFFB6569B);

  //SECONDARY2
  static const Color Secondary2A = Color(0xFF2A085C);
  static const Color Secondary2B = Color(0xFF291645);
  static const Color Secondary2C = Color(0xFF1A033C);
  static const Color Secondary2D = Color(0xFF6A3AB2);
  static const Color Secondary2E = Color(0xFF7E5CB2);

}