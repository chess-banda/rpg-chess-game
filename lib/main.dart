import 'dart:ffi';
import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rpgchessgame/CustomColors.dart';

import 'package:rpgchessgame/Localization.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Deck.dart';
import 'package:rpgchessgame/views/allDecksManagementMenu.dart';
import 'package:rpgchessgame/views/startGameMenu.dart';

import 'CustomMaterialAppClass.dart';
import 'CustomWidgets.dart';


enum Exit{
  Yes,
  No
}



void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  ProviderAllDecks.tryLoadFromFile();
  if(ProviderAllDecks.getInst().decks.length == 0)
    ProviderAllDecks.getInst().addDefaultDeck();

  runApp(LocalizedLandscapeMaterialApp(home: MainMenu()));


  return;
}



class MainMenu extends StatelessWidget {
  final Color buttonColor = CustomColors.Main1C;
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    double titleSize = h * 0.13;
    double fontSize = h * 0.05;
    double buttonH = h * 0.13;
    double buttonW = w * 0.4;

    return Scaffold(
        backgroundColor: CustomColors.DarkA,
        body: Stack(
          children: <Widget>[
            Image.asset(
              "assets/main_menu/back.png",
              alignment: Alignment.center,
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.cover,
            ),
            Center(
              child: Column(
                children: <Widget>[
                  Spacer(flex: 1,),
                  SimpleText(
                    text: GLocalizations.of(context).mainMenuTextNameOfGame,
                    color: CustomColors.DarkA,
                    size: titleSize,
                  ),
                  Spacer(flex: 5,),
                  SimpleButton(
                    child: FlatButton(
                      color: buttonColor,
                      onPressed: _procStartGame(context),
                      child: SimpleText(
                        text: GLocalizations.of(context).mainMenuButtonStartGame,
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ),
                    minWidth: buttonW,
                    height: buttonH,
                  ),
                  Spacer(flex: 1,),
                  SimpleButton(
                    child: FlatButton(
                      color: buttonColor,
                      onPressed: _procHelp(context),
                      child: SimpleText(
                        text: GLocalizations.of(context).mainMenuButtonHelp,
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ),
                    minWidth: buttonW,
                    height: buttonH,
                  ),
                  Spacer(flex: 1,),
                  SimpleButton(
                    child: FlatButton(
                      color: buttonColor,
                      onPressed: _procDeckEditor(context),
                      child: SimpleText(
                        text: GLocalizations.of(context).mainMenuButtonDeckManagement,
                        color: CustomColors.LightA,
                        size: fontSize,
                      ),
                    ),
                    minWidth: buttonW,
                    height: buttonH,
                  ),
                  Spacer(flex: 5,),
                  Row(
                    children: <Widget>[
                      Spacer(flex: 1),
                      SimpleBoxButton(
                        icon: Icon(Icons.exit_to_app, color: CustomColors.LightA,),
                        onTap: _procExit(context),
                        minWidth: buttonH,
                        height: buttonH,
                      ),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
    );
  }
}


VoidCallback _procHelp(BuildContext context) {
  return () {


  };
}

VoidCallback _procStartGame(BuildContext context) {
  return () {
    Navigator.push(
        context, MaterialPageRoute(
        builder: (context) => StartGame()
    )
    );
  };
}

VoidCallback _procDeckEditor(BuildContext context) {
  return () {
    Navigator.push(
        context, MaterialPageRoute(
        builder: (context) => AllDecksManagementMenu(selectMode: false)
    )
    );
  };
}

VoidCallback _procExit(BuildContext context) {
  return () async {
    double fontSize = MediaQuery.of(context).size.height * 0.06;
    switch(await showDialog<Exit>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: CustomColors.Main1C,
            title: Center(
              child: SimpleText(
                text: GLocalizations.of(context).mainMenuTextExitDialogTitle,
                color: CustomColors.LightA,
                size: fontSize,
              ),
            ),
            children: <Widget>[
              Row(
                children: <Widget>[
                  Spacer(flex: 1,),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, Exit.Yes);
                    },
                    child: SimpleText(
                      text: GLocalizations.of(context).mainMenuTextExitDialogYes,
                      color: CustomColors.LightA,
                      size: fontSize,
                    ),
                  ),
                  Spacer(flex: 1,),
                  SimpleDialogOption(
                    onPressed: () {
                      Navigator.pop(context, Exit.No);
                    },
                    child: SimpleText(
                      text: GLocalizations.of(context).mainMenuTextExitDialogNo,
                      color: CustomColors.LightA,
                      size: fontSize,
                    ),
                  ),
                  Spacer(flex: 1,),
                ],
              ),

            ],
          );
        })) {
      case Exit.Yes:
        {
          SystemNavigator.pop();
          await ProviderAllDecks.saveToFile();
        }
        break;
      case Exit.No:
        {

        }
        break;
    }
  };
}