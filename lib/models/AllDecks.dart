import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:path_provider/path_provider.dart';
import '../models/figures/FigureBase.dart';
import 'package:rpgchessgame/CustomWidgets.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';
import 'package:rpgchessgame/models/figures/bishop/Mage.dart';
import 'package:rpgchessgame/models/figures/bishop/Warrior.dart';
import 'package:rpgchessgame/models/figures/king/Caster.dart';
import 'package:rpgchessgame/models/figures/king/Palladin.dart';
import 'package:rpgchessgame/models/figures/king/Strategist.dart';
import 'package:rpgchessgame/models/figures/knight/Rider.dart';
import 'package:rpgchessgame/models/figures/pawn/Archer.dart';
import 'package:rpgchessgame/models/figures/pawn/SwordsMen.dart';
import 'package:rpgchessgame/models/figures/queen/ArchMage.dart';
import 'package:rpgchessgame/models/figures/queen/Assassin.dart';
import 'package:rpgchessgame/models/figures/queen/Champion.dart';
import 'package:rpgchessgame/models/figures/rock/Berserk.dart';
import 'package:rpgchessgame/models/figures/rock/Shielder.dart';
import '../CustomColors.dart';
import 'Deck.dart';


part 'AllDecks.g.dart';

class DeckDrawer {
  static Widget getPawnAvatar(PawnId id) {
    switch(id) {
      case PawnId.Archer: return Archer.avatar;
      case PawnId.SwordsMen: return SwordsMen.avatar;
    }
  }

  static Widget getBishopAvatar(BishopId id) {
    switch(id) {
      case BishopId.Mage: return Mage.avatar;
      case BishopId.Warrior: return Warrior.avatar;
    }
  }

  static Widget getKingAvatar(KingId id) {
    switch(id) {
      case KingId.Strategist: return Strategist.avatar;
      case KingId.Caster: return Caster.avatar;
      case KingId.Palladin: return Palladin.avatar;
    }
  }

  static Widget getKnightAvatar(KnightId id) {
    switch(id) {
      case KnightId.Rider: return Rider.avatar;
    }
  }

  static Widget getQueenAvatar(QueenId id) {
    switch(id) {
      case QueenId.Assassin: return Assassin.avatar;
      case QueenId.ArchMage: return ArchMage.avatar;
      case QueenId.Champion: return Champion.avatar;
    }
  }

  static Widget getRockAvatar(RockId id) {
    switch(id) {
      case RockId.Berserk: return Berserk.avatar;
      case RockId.Shielder: return Shielder.avatar;
    }
  }




  static Widget getPawnAvatarWtf(dynamic id) {
    switch(id) {
      case PawnId.Archer: return Archer.avatarWtf;
      case PawnId.SwordsMen: return SwordsMen.avatarWtf;
    }
  }

  static Widget getBishopAvatarWtf(dynamic id) {
    switch(id) {
      case BishopId.Mage: return Mage.avatarWtf;
      case BishopId.Warrior: return Warrior.avatarWtf;
    }
  }

  static Widget getKingAvatarWtf(dynamic id) {
    switch(id) {
      case KingId.Strategist: return Strategist.avatarWtf;
      case KingId.Caster: return Caster.avatarWtf;
      case KingId.Palladin: return Palladin.avatarWtf;
    }
  }

  static Widget getKnightAvatarWtf(dynamic id) {
    switch(id) {
      case KnightId.Rider: return Rider.avatarWtf;
    }
  }

  static Widget getQueenAvatarWtf(dynamic id) {
    switch(id) {
      case QueenId.Assassin: return Assassin.avatarWtf;
      case QueenId.ArchMage: return ArchMage.avatarWtf;
      case QueenId.Champion: return Champion.avatarWtf;
    }
  }

  static Widget getRockAvatarWtf(dynamic id) {
    switch(id) {
      case RockId.Berserk: return Berserk.avatarWtf;
      case RockId.Shielder: return Shielder.avatarWtf;
    }
  }




  static Widget getRockIcon(RockId id, FigureTeam teamId) {
    switch(id) {
      case RockId.Berserk:
        switch (teamId) {
          case FigureTeam.Black: return Berserk.icon_b;
          case FigureTeam.White: return Berserk.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case RockId.Shielder:
        switch (teamId) {
          case FigureTeam.Black: return Shielder.icon_b;
          case FigureTeam.White: return Shielder.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;

    }
  }

  static Widget getPawnIcon(PawnId id, FigureTeam teamId) {
    switch(id) {
      case PawnId.Archer:
        switch (teamId) {
          case FigureTeam.Black: return Archer.icon_b;
          case FigureTeam.White: return Archer.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case PawnId.SwordsMen:
        switch (teamId) {
          case FigureTeam.Black: return SwordsMen.icon_b;
          case FigureTeam.White: return SwordsMen.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
    }
  }

  static Widget getBishopIcon(BishopId id, FigureTeam teamId) {
    switch(id) {
      case BishopId.Mage:
        switch (teamId) {
          case FigureTeam.Black: return Mage.icon_b;
          case FigureTeam.White: return Mage.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case BishopId.Warrior:
        switch (teamId) {
          case FigureTeam.Black: return Warrior.icon_b;
          case FigureTeam.White: return Warrior.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
    }
  }

  static Widget getKingIcon(KingId id, FigureTeam teamId) {
    switch(id) {
      case KingId.Strategist:
        switch (teamId) {
          case FigureTeam.Black: return Strategist.icon_b;
          case FigureTeam.White: return Strategist.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case KingId.Caster:
        switch (teamId) {
          case FigureTeam.Black: return Strategist.icon_b;
          case FigureTeam.White: return Strategist.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case KingId.Palladin:
        switch (teamId) {
          case FigureTeam.Black: return Palladin.icon_b;
          case FigureTeam.White: return Palladin.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
    }
  }

  static Widget getKnightIcon(KnightId id, FigureTeam teamId) {
    switch(id) {
      case KnightId.Rider:
        switch (teamId) {
          case FigureTeam.Black: return Rider.icon_b;
          case FigureTeam.White: return Rider.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
    }
  }

  static Widget getQueenIcon(QueenId id, FigureTeam teamId) {
    switch(id) {
      case QueenId.Assassin:
        switch (teamId) {
          case FigureTeam.Black: return Assassin.icon_b;
          case FigureTeam.White: return Assassin.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case QueenId.ArchMage:
        switch (teamId) {
          case FigureTeam.Black: return ArchMage.icon_b;
          case FigureTeam.White: return ArchMage.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
      case QueenId.Champion:
        switch (teamId) {
          case FigureTeam.Black: return Champion.icon_b;
          case FigureTeam.White: return Champion.icon_w;
          case FigureTeam.Red:
          case FigureTeam.Green:
        }
        break;
    }
  }

  static Widget getFigureAvatarWtf(dynamic id) {
    if(id is BishopId) return getBishopAvatarWtf(id);
    else if(id is KingId) return getKingAvatarWtf(id);
    else if(id is QueenId) return getQueenAvatarWtf(id);
    else if(id is KnightId) return getKnightAvatarWtf(id);
    else if(id is RockId) return getRockAvatarWtf(id);
    else if(id is PawnId) return getPawnAvatarWtf(id);
  }


  static Widget getFigureAvatar(dynamic id) {
    if(id is BishopId) return getBishopAvatar(id);
    else if(id is KingId) return getKingAvatar(id);
    else if(id is QueenId) return getQueenAvatar(id);
    else if(id is KnightId) return getKnightAvatar(id);
    else if(id is RockId) return getRockAvatar(id);
    else if(id is PawnId) return getPawnAvatar(id);
  }

  static Widget _drawDeckGridRedoPiece(
      Widget img,
      Widget icon,
      double iconwh) {

    return Stack(
      children: <Widget>[
        Container(
          child: img,
        ),
        Column(
          children: <Widget>[
            Spacer(),
            Row(
              children: <Widget>[
                Spacer(),
                Container(
                  child: icon,
                  width: iconwh, height: iconwh,
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  static Widget drawDeckGridRedo(Deck deck, double h) {

    return GridView.count(
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 8,
      padding: EdgeInsets.all(3),
      mainAxisSpacing: 3,
      crossAxisSpacing: 3,
      children: <Widget>[
        // ignore: sdk_version_ui_as_code
        for(PawnId id in deck.pawns)
          _drawDeckGridRedoPiece(
              getPawnAvatar(id), getPawnIcon(id, FigureTeam.Black), h * 0.4
          ),

        _drawDeckGridRedoPiece(
            getRockAvatar(deck.rocks[0]), getRockIcon(deck.rocks[0], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getKnightAvatar(deck.knights[0]), getKnightIcon(deck.knights[0], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getBishopAvatar(deck.bishops[0]), getBishopIcon(deck.bishops[0], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getKingAvatar(deck.kings[0]), getKingIcon(deck.kings[0], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getQueenAvatar(deck.queens[0]), getQueenIcon(deck.queens[0], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getBishopAvatar(deck.bishops[1]), getBishopIcon(deck.bishops[1], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getKnightAvatar(deck.knights[1]), getKnightIcon(deck.knights[1], FigureTeam.Black), h * 0.4
        ),

        _drawDeckGridRedoPiece(
            getRockAvatar(deck.rocks[1]), getRockIcon(deck.rocks[1], FigureTeam.Black), h * 0.4
        ),
      ],
    );
  }

  static Widget drawCard({
    @required Widget image,
    @required double h,
    @required String name}) {
    return Container(
      width: 0.75 * h,
      height: h,
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Image.asset("assets/frame/frame.png"),
          Container(
            width: 0.75 * h,
            height: h,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 2,),
                Container(
                  width: 0.6 * h,
                  height: 0.6 * h,
                  child: image,
                ),
                Spacer(flex: 1,),
                FigureNameText(
                  size: 0.07 * h,
                  color: CustomColors.DarkE,
                  text: name,
                ),
                Spacer(flex: 5,),
              ],
            ),
          ),
        ],
      ),
    );
  }
}





@JsonSerializable()
class _AllDecks {

  List<Deck> decks = List<Deck>();

  _AllDecks();

  void addDeck(Deck newDeck) {
    decks.add(newDeck);

  }

  void addDefaultDeck() {
    decks.add(Deck());
  }

  void removeDeck(int idx) {
    decks.removeAt(idx);
  }
  void setDeckName(int idx, String name) {
    decks[idx].name = name;
  }

  String getDeckName(int idx) {
    return decks[idx].name;
  }

  Map<FigureBaseType, List<FigureBase>> deckToFigureArray(int idx) {
    Map<FigureBaseType, List<FigureBase>> array = Map<FigureBaseType, List<FigureBase>>();
    array[FigureBaseType.Bishop] = List<IBishop>();
    array[FigureBaseType.King] = List<IKing>();
    array[FigureBaseType.Knight] = List<IKnight>();
    array[FigureBaseType.Pawn] = List<IPawn>();
    array[FigureBaseType.Queen] = List<IQueen>();
    array[FigureBaseType.Rock] = List<IRock>();

    for(BishopId id in decks[idx].bishops) {
      array[FigureBaseType.Bishop].add(ProviderAllDecks.getBishopClassFromId(id));
    }

    for(KingId id in decks[idx].kings) {
      array[FigureBaseType.King].add(ProviderAllDecks.getKingClassFromId(id));
    }

    for(QueenId id in decks[idx].queens) {
      array[FigureBaseType.Queen].add(ProviderAllDecks.getQueenClassFromId(id));
    }

    for(RockId id in decks[idx].rocks) {
      array[FigureBaseType.Rock].add(ProviderAllDecks.getRockClassFromId(id));
    }

    for(PawnId id in decks[idx].pawns) {
      array[FigureBaseType.Pawn].add(ProviderAllDecks.getPawnClassFromId(id));
    }

    for(KnightId id in decks[idx].knights) {
      array[FigureBaseType.Knight].add(ProviderAllDecks.getKnightClassFromId(id));
    }

    return array;
  }

  factory _AllDecks.fromJson(Map<String, dynamic> json) => _$_AllDecksFromJson(json);
  Map<String, dynamic> toJson() => _$_AllDecksToJson(this);
}









class ProviderAllDecks {
  static _AllDecks _instance;
  static String _fileName = "alldecks.json";
  static String _path = "";

  static Future<void> getAppPath() async {
    Directory tempDir = await getApplicationDocumentsDirectory();
    _path = tempDir.path;
    print(_path);
  }

  static Future<void> tryLoadFromFile() async {
    await getAppPath();
    try {
      _instance = _AllDecks.fromJson(
          json.decode(await File('$_path/$_fileName').readAsString()));
    }
    catch (e) {
      print(e.toString());
      _instance = _AllDecks();
    }
  }

  static Future<void> saveToFile() async {

    await File('$_path/$_fileName').writeAsString(json.encode(_instance.toJson()));
  }

  static _AllDecks getInst() {
    if(_instance == null) _instance = _AllDecks();
    return _instance;
  }


  static IQueen getQueenClassFromId(dynamic id) {
    switch(id) {
      case QueenId.ArchMage: return ArchMage();
      case QueenId.Assassin: return Assassin();
      case QueenId.Champion: return Champion();
    }
  }

  static IKing getKingClassFromId(dynamic id) {
    switch (id) {
      case KingId.Palladin: return Palladin();
      case KingId.Strategist: return Strategist();
      case KingId.Caster: return Caster();
    }
  }
  static IKnight getKnightClassFromId(dynamic id) {
    switch(id) {
      case KnightId.Rider: return Rider();
    }
  }

  static IPawn getPawnClassFromId(dynamic id) {
    switch(id) {
      case PawnId.SwordsMen: return SwordsMen();
      case PawnId.Archer: return Archer();
    }
  }

  static IBishop getBishopClassFromId(dynamic id) {
    switch(id) {
      case BishopId.Warrior: return Warrior();
      case BishopId.Mage: return Mage();
    }
  }

  static IRock getRockClassFromId(dynamic id) {
    switch(id) {
      case RockId.Shielder: return Shielder();
      case RockId.Berserk: return Berserk();
    }
  }

  static FigureBase getFigureClass(dynamic id) {
    if(id is BishopId) return getBishopClassFromId(id);
    else if(id is KingId) return getKingClassFromId(id);
    else if(id is QueenId) return getQueenClassFromId(id);
    else if(id is KnightId) return getKnightClassFromId(id);
    else if(id is RockId) return getRockClassFromId(id);
    else if(id is PawnId) return getPawnClassFromId(id);
  }

}




