
import 'dart:ui';

import 'package:flutter/cupertino.dart';

import 'Field.dart';
import 'abilities/AbilityBase.dart';
import 'figures/FigureBase.dart';

class Cell {
  Key _cellKey;

  FigureBase _figure;
  List<IBuff> _buff;
  List<IDeBuff> _debuff;

  final Widget texture;


  Cell({@required this.texture}) : _cellKey = UniqueKey();

  Cell.empty({@required Widget texture})
      : this.texture = Opacity(
    opacity: 0.0,
    child: texture,
  );

  void tick() {


  }

  Widget drawCell(Widget img) {
    return Stack(
      children: <Widget>[
        texture,
        img,
        if(_figure != null) _figure.drawOnField(),
      ],
    );
  }


  FigureBase get figure => _figure;
  set figure(value) => _figure = value;

}

