// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Deck.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Deck _$DeckFromJson(Map<String, dynamic> json) {
  return Deck()
    ..name = json['name'] as String
    ..pawns = (json['pawns'] as List)
        ?.map((e) => _$enumDecodeNullable(_$PawnIdEnumMap, e))
        ?.toList()
    ..bishops = (json['bishops'] as List)
        ?.map((e) => _$enumDecodeNullable(_$BishopIdEnumMap, e))
        ?.toList()
    ..knights = (json['knights'] as List)
        ?.map((e) => _$enumDecodeNullable(_$KnightIdEnumMap, e))
        ?.toList()
    ..rocks = (json['rocks'] as List)
        ?.map((e) => _$enumDecodeNullable(_$RockIdEnumMap, e))
        ?.toList()
    ..queens = (json['queens'] as List)
        ?.map((e) => _$enumDecodeNullable(_$QueenIdEnumMap, e))
        ?.toList()
    ..kings = (json['kings'] as List)
        ?.map((e) => _$enumDecodeNullable(_$KingIdEnumMap, e))
        ?.toList();
}

Map<String, dynamic> _$DeckToJson(Deck instance) => <String, dynamic>{
      'name': instance.name,
      'pawns': instance.pawns?.map((e) => _$PawnIdEnumMap[e])?.toList(),
      'bishops': instance.bishops?.map((e) => _$BishopIdEnumMap[e])?.toList(),
      'knights': instance.knights?.map((e) => _$KnightIdEnumMap[e])?.toList(),
      'rocks': instance.rocks?.map((e) => _$RockIdEnumMap[e])?.toList(),
      'queens': instance.queens?.map((e) => _$QueenIdEnumMap[e])?.toList(),
      'kings': instance.kings?.map((e) => _$KingIdEnumMap[e])?.toList(),
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$PawnIdEnumMap = {
  PawnId.Archer: 'Archer',
  PawnId.SwordsMen: 'SwordsMen',
};

const _$BishopIdEnumMap = {
  BishopId.Mage: 'Mage',
  BishopId.Warrior: 'Warrior',
};

const _$KnightIdEnumMap = {
  KnightId.Rider: 'Rider',
};

const _$RockIdEnumMap = {
  RockId.Berserk: 'Berserk',
  RockId.Shielder: 'Shielder',
};

const _$QueenIdEnumMap = {
  QueenId.ArchMage: 'ArchMage',
  QueenId.Assassin: 'Assassin',
  QueenId.Champion: 'Champion',
};

const _$KingIdEnumMap = {
  KingId.Caster: 'Caster',
  KingId.Palladin: 'Palladin',
  KingId.Strategist: 'Strategist',
};
