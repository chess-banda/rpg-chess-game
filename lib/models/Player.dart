


import 'package:flutter/cupertino.dart';

import '../models/figures/FigureBase.dart';


class Player {
  String _name;
  Map<FigureBaseType, List<FigureBase>> _figures;
  FigureTeam _teamId = FigureTeam.White;
  DateTime timer;
  int selectedDeckIdx = -1;


  Player({@required String name}) : _name = name;

  String get name => _name;
  set name(value) => _name = value;

  Map<FigureBaseType, List<FigureBase>> get figures => _figures;
  set figures(value) => _figures = value;

  FigureTeam get teamId => _teamId;
  set teamId(value) => _teamId = value;
}