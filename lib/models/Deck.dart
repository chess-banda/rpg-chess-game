import 'package:json_annotation/json_annotation.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';

part 'Deck.g.dart';

@JsonSerializable()

class Deck {
  String name = "Default deck";
  List<PawnId> pawns = List<PawnId>.filled(_pawnMaxCount, PawnId.Archer);
  List<BishopId> bishops = List<BishopId>.filled(_bishopMaxCount, BishopId.Mage);
  List<KnightId> knights = List<KnightId>.filled(_knightMaxCount, KnightId.Rider);
  List<RockId> rocks = List<RockId>.filled(_rockMaxCount, RockId.Berserk);
  List<QueenId> queens = List<QueenId>.filled(_queenMaxCount, QueenId.ArchMage);
  List<KingId> kings = List<KingId>.filled(_kingMaxCount, KingId.Strategist);



  static int _pawnMaxCount = 8;
  static int _bishopMaxCount = 2;
  static int _knightMaxCount = 2;
  static int _rockMaxCount = 2;
  static int _queenMaxCount = 1;
  static int _kingMaxCount = 1;

  Deck();

  Deck clone() {
    Deck deck = Deck();
    deck.name = name.substring(0);
    deck.rocks = List.from(rocks);
    deck.kings = List.from(kings);
    deck.queens = List.from(queens);
    deck.bishops = List.from(bishops);
    deck.pawns = List.from(pawns);
    deck.knights = List.from(knights);

    return deck;
  }

  void replaceKingAt(int idx, KingId id) => kings[idx] = id;
  void replaceQueenAt(int idx, QueenId id) => queens[idx] = id;
  void replacePawnAt(int idx, PawnId id) => pawns[idx] = id;
  void replaceBishopAt(int idx, BishopId id) => bishops[idx] = id;
  void replaceRockAt(int idx, RockId id) => rocks[idx] = id;
  void replaceKnightAt(int idx, KnightId id) => knights[idx] = id;

  factory Deck.fromJson(Map<String, dynamic> json) => _$DeckFromJson(json);
  Map<String, dynamic> toJson() => _$DeckToJson(this);
}