// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AllDecks.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_AllDecks _$_AllDecksFromJson(Map<String, dynamic> json) {
  return _AllDecks()
    ..decks = (json['decks'] as List)
        ?.map(
            (e) => e == null ? null : Deck.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$_AllDecksToJson(_AllDecks instance) => <String, dynamic>{
      'decks': instance.decks,
    };
