


import 'package:flutter/material.dart';


import '../Cell.dart';
import '../Field.dart';

abstract class AbilityBase {
  String _name;
  String _description;
  Image icon;

  void use(Field field);
  void loadConfig();
  void drawIconInMenu();
  void drawIconInGame();
  void tick(Field field);
  void useOnCell(Cell cell);
}


abstract class IAtack extends AbilityBase {


}


abstract class IBuff extends AbilityBase {



}


abstract class IDeBuff extends AbilityBase {



}