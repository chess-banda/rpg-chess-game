

import 'dart:math';


import 'package:flutter/widgets.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Field.dart';
import 'package:rpgchessgame/models/abilities/AbilityBase.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';

import '../../../CustomColors.dart';
import '../../../FigureLocalizations.dart';
import '../Config.dart';

class Mage implements IBishop {
  static Image icon_b = Image.asset('assets/icons/bishop/mage_b.png');
  static Image icon_w = Image.asset('assets/icons/bishop/mage_w.png');

  static Image avatar = Image.asset('assets/avatars/bishop/mage.png');
  static Image avatarWtf = Image.asset('assets/avatars/bishop/mage_wtf.png');

  @override
  List<AbilityBase> abilities;

  @override
  List<Point<num>> moves;

  @override
  FigureStatus status;

  @override
  double armor;

  @override
  double atackDamage;

  @override
  double health;

  @override
  FigureTeam team;

  @override
  void doMove(Field field) {
    // TODO: implement doMove
  }

  @override
  void drawMoves(Field field) {
    // TODO: implement drawMoves
  }

  @override
  Widget drawOnField() {
    return DeckDrawer.getBishopIcon(BishopId.Mage, team);
  }

  Mage() {
    FigureConfigTemplate config = ProvideFiguresConfig.mageConfig;
    health = config.health;
    armor = config.armor;
    atackDamage = config.atackDamage;
    abilities = config.abilities;
    moves = config.moves;
    maxHPAndArmor = config.health;
  }

  @override
  double maxHPAndArmor;

  @override
  var id = BishopId.Mage;



}