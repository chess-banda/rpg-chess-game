import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:rpgchessgame/models/Field.dart';
import 'package:rpgchessgame/models/abilities/AbilityBase.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';

import '../../../CustomColors.dart';
import '../../../FigureLocalizations.dart';
import '../../AllDecks.dart';
import '../Config.dart';

class SwordsMen implements IPawn {
  static Image icon_b = Image.asset('assets/icons/pawn/swordsmen_b.png');
  static Image icon_w = Image.asset('assets/icons/pawn/swordsmen_w.png');

  static Image avatar = Image.asset('assets/avatars/pawn/swordsmen.png');
  static Image avatarWtf = Image.asset('assets/avatars/pawn/swordsmen_wtf.png');

  @override
  List<AbilityBase> abilities;

  @override
  double armor;

  @override
  double atackDamage;

  @override
  double health;

  @override
  List<Point<num>> moves;

  @override
  FigureStatus status;

  @override
  FigureTeam team;

  @override
  void doMove(Field field) {
    // TODO: implement doMove
  }

  @override
  void drawMoves(Field field) {
    // TODO: implement drawMoves
  }

  @override
  Widget drawOnField() {

    return DeckDrawer.getPawnIcon(PawnId.SwordsMen, team);
  }
  //Object class overrides

  SwordsMen() {
    SwordsMenConfig config = ProvideFiguresConfig.swordsMenConfig;
    health = config.health;
    armor = config.armor;
    atackDamage = config.atackDamage;
    abilities = config.abilities;
    maxHPAndArmor = config.health;
  }

  @override
  double maxHPAndArmor;
  @override
  bool firstMove = true;


  @override
  var id = PawnId.SwordsMen;

}