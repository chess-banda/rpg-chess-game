


import 'dart:math';


import 'package:flutter/cupertino.dart';
import 'package:rpgchessgame/models/Field.dart';
import 'package:rpgchessgame/models/abilities/AbilityBase.dart';


enum FigureBaseType {
  Bishop,
  King,
  Knight,
  Pawn,
  Queen,
  Rock
}

enum BishopId {
  Mage,
  Warrior,
}
enum KingId {
  Caster,
  Palladin,
  Strategist,
}
enum KnightId {
  Rider,
}
enum PawnId {
  Archer,
  SwordsMen,
}
enum QueenId {
  ArchMage,
  Assassin,
  Champion,
}
enum RockId {
  Berserk,
  Shielder,
}

enum FigureStatus {
  Killed,
  Alive,
}

enum FigureTeam {
  Black,
  White,
  Red,
  Green,
}


abstract class FigureBase {
  double health, armor, atackDamage, maxHPAndArmor;
  List<Point> moves;
  List<AbilityBase> abilities;
  FigureStatus status;
  FigureTeam team;
  var id;


  void doMove(Field field);
  void drawMoves(Field field);
  Widget drawOnField();

}



abstract class IBishop extends FigureBase {



}

abstract class IKing extends FigureBase {
  bool firstMove;


}

abstract class IKnight extends FigureBase {



}


abstract class IPawn extends FigureBase {
  bool firstMove;


}


abstract class IQueen extends FigureBase {



}

abstract class IRock extends FigureBase {
  bool firstMove;


}
