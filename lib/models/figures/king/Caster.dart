import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:rpgchessgame/FigureLocalizations.dart';
import 'package:rpgchessgame/models/AllDecks.dart';
import 'package:rpgchessgame/models/Field.dart';
import 'package:rpgchessgame/models/abilities/AbilityBase.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';

import '../../../CustomColors.dart';
import '../Config.dart';

class Caster implements IKing {
  static Image icon_b = Image.asset('assets/icons/king/caster_b.png');
  static Image icon_w = Image.asset('assets/icons/king/caster_w.png');

  static Image avatar = Image.asset('assets/avatars/king/caster.png');
  static Image avatarWtf = Image.asset('assets/avatars/king/caster_wtf.png');

  @override
  List<AbilityBase> abilities;

  @override
  double armor;

  @override
  double atackDamage;

  @override
  double health;

  @override
  List<Point<num>> moves;

  @override
  FigureStatus status;

  @override
  FigureTeam team;

  @override
  void doMove(Field field) {
    // TODO: implement doMove
  }

  @override
  void drawMoves(Field field) {
    // TODO: implement drawMoves
  }

  @override
  Widget drawOnField() {
    return DeckDrawer.getKingIcon(KingId.Caster, team);
  }
  //Object class overrides

  Caster() {
    FigureConfigTemplate config = ProvideFiguresConfig.casterConfig;
    health = config.health;
    armor = config.armor;
    atackDamage = config.atackDamage;
    abilities = config.abilities;
    moves = config.moves;
    maxHPAndArmor = config.health;
  }

  @override
  double maxHPAndArmor;
  @override
  bool firstMove = true;

  @override
  var id = KingId.Caster;


}