

import 'dart:math';
import 'package:rpgchessgame/models/abilities/AbilityBase.dart';

enum Direction {
  up,
  right,
  down,
  left,
}

int _infinity = 17;

List<Point> _generatePointsV1(int maxLen) {
  List<Point> points = List<Point>();
  for(int i = 1; i <= maxLen; ++i) {
    points.add(Point(i,i));
    points.add(Point(-i,-i));
    points.add(Point(-i,i));
    points.add(Point(i,-i));
    points.add(Point(i,0));
    points.add(Point(-i,0));
    points.add(Point(0,i));
    points.add(Point(0,-i));
  }
  return points;
}


abstract class FigureConfigTemplate{
  List<AbilityBase> abilities;
  double armor;
  double atackDamage;
  double health;
  List<Point<num>> moves;
}


class MageConfig implements FigureConfigTemplate{
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 2;
  @override
  double health = 1;
  @override
  List<Point<num>> moves = [
    for(int i = 1; i < _infinity; ++i)
      Point(i,i),

    for(int i = 1; i < _infinity; ++i)
      Point(i,-i),

    for(int i = 1; i < _infinity; ++i)
      Point(-i,i),

    for(int i = 1; i < _infinity; ++i)
      Point(-i,-i),
  ];
}



class WarriorConfig implements FigureConfigTemplate{
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 2;
  @override
  double health = 2;
  @override
  List<Point<num>> moves = [
    Point(1,1),
    Point(2,2),
    Point(3,3),

    Point(-1,-1),
    Point(-2,-2),
    Point(-3,-3),

    Point(1,-1),
    Point(2,-2),
    Point(3,-3),

    Point(-1,1),
    Point(-2,2),
    Point(-3,3),


  ];
}



class CasterConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 2;
  @override
  double health = 5;
  @override
  List<Point<num>> moves = [
    Point(1,1),
    Point(0,1),
    Point(1,0),
    Point(-1,-1),
    Point(0,-1),
    Point(-1,0),
    Point(1,-1),
    Point(-1,1),
  ];
}


class PalladinConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 3;
  @override
  double health = 5;
  @override
  List<Point<num>> moves = [
    Point(1,1),
    Point(0,1),
    Point(1,0),
    Point(-1,-1),
    Point(0,-1),
    Point(-1,0),
    Point(1,-1),
    Point(-1,1),
  ];

}


class StrategistConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 1;
  @override
  double health = 5;
  @override
  List<Point<num>> moves = [
    Point(1,1),
    Point(0,1),
    Point(1,0),
    Point(-1,-1),
    Point(0,-1),
    Point(-1,0),
    Point(1,-1),
    Point(-1,1),
  ];

}


class RiderConfig implements FigureConfigTemplate
{
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 2;
  @override
  double health = 1;
  @override
  List<Point<num>> moves = [
    Point(2,1),
    Point(2,-1),

    Point(-1,2),
    Point(1,2),

    Point(-2,1),
    Point(-2,-1),

    Point(-1,-2),
    Point(1,-2),
  ];
}





class ArcherConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 1;
  @override
  double health = 1;
  @override
  List<Point<num>> moves = [];
  Map<Direction, List<Point>> directedMoves = {
    Direction.up: <Point> [
      Point(0,-1),
      Point(0,-2),

      Point(1,-1),
      Point(-1,-1),
    ],

    Direction.down: <Point> [
      Point(0,1),
      Point(0,2),

      Point(1, 1),
      Point(-1, 1),
    ],

    Direction.left: <Point> [
      Point(-1, 0),
      Point(-2, 0),

      Point(-1, -1),
      Point(-1, 1),
    ],

    Direction.right: <Point> [
      Point(1, 0),
      Point(2, 0),

      Point(1, -1),
      Point(1, 1),
    ],
  };
}






class SwordsMenConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 1;
  @override
  double health = 2;
  @override
  List<Point<num>> moves = [];
  Map<Direction, List<Point>> directedMoves = {
    Direction.up: <Point> [
      Point(0,-1),
      Point(0,-2),


      Point(1,-1),
      Point(2,-2),
      Point(-1,-1),
      Point(-2,-2),
    ],

    Direction.down: <Point> [
      Point(0,1),
      Point(0,2),

      Point(1, 1),
      Point(2, 2),
      Point(-2, 2),
      Point(-1, 1),
    ],

    Direction.left: <Point> [
      Point(-1,0),
      Point(-2,0),

      Point(-1, -1),
      Point(-2, -2),
      Point(-1, 1),
      Point(-2, 2),
    ],

    Direction.right: <Point> [
      Point(1,0),
      Point(2,0),

      Point(1, -1),
      Point(2, -2),
      Point(1, 1),
      Point(2, 2),
    ],
  };
}






class ArchMageConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 3;
  @override
  double health = 2;
  @override
  List<Point<num>> moves = _generatePointsV1(_infinity);
}







class AssassinConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 3;
  @override
  double health = 2;
  @override
  List<Point<num>> moves = _generatePointsV1(4);

}







class ChampionConfig implements FigureConfigTemplate {

  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 3;
  @override
  double health = 3;
  @override
  List<Point<num>> moves = _generatePointsV1(4);

}







class BerserkConfig implements FigureConfigTemplate {
  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 3;
  @override
  double health = 2;
  @override
  List<Point<num>> moves = [
    Point(1,0),
    Point(2,0),
    Point(3,0),

    Point(-1,0),
    Point(-2,0),
    Point(-3,0),

    Point(0,1),
    Point(0,2),
    Point(0,3),

    Point(0,-1),
    Point(0,-2),
    Point(0,-3),
  ];
}





class ShielderConfig implements FigureConfigTemplate {

  @override
  List<AbilityBase> abilities = [];
  @override
  double armor = 0;
  @override
  double atackDamage = 2;
  @override
  double health = 4;
  @override
  List<Point<num>> moves = [
    Point(1,0),
    Point(2,0),
    Point(3,0),

    Point(-1,0),
    Point(-2,0),
    Point(-3,0),

    Point(0,1),
    Point(0,2),
    Point(0,3),

    Point(0,-1),
    Point(0,-2),
    Point(0,-3),
  ];

}



class ProvideFiguresConfig {
  static MageConfig get mageConfig => MageConfig();
  static WarriorConfig get warriorConfig => WarriorConfig();
  static PalladinConfig get palladinConfig => PalladinConfig();
  static CasterConfig get casterConfig => CasterConfig();
  static StrategistConfig get strategistConfig => StrategistConfig();
  static RiderConfig get riderConfig => RiderConfig();
  static ArcherConfig get archerConfig => ArcherConfig();
  static SwordsMenConfig get swordsMenConfig => SwordsMenConfig();
  static ArchMageConfig get archMageConfig => ArchMageConfig();
  static AssassinConfig get assassinConfig => AssassinConfig();
  static ChampionConfig get championConfig => ChampionConfig();
  static BerserkConfig get berserkConfig => BerserkConfig();
  static ShielderConfig get shielderConfig => ShielderConfig();
}






