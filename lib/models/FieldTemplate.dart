


import 'dart:math';

import 'package:rpgchessgame/models/figures/FigureBase.dart';

enum CellT{
  black,
  white,
  empty,
}

abstract class FieldTemplate {
  int maxW, maxH;
  List<List<CellT>> map;
  Map<FigureTeam, Map<FigureBaseType, List<Point>>> points;
}


class DefaultField implements FieldTemplate{
  @override
  List<List<CellT>> map = const <List<CellT>>[
    const<CellT>[CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black],
    const<CellT>[CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white],
    const<CellT>[CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black],
    const<CellT>[CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white],
    const<CellT>[CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black],
    const<CellT>[CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white],
    const<CellT>[CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black],
    const<CellT>[CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white, CellT.black, CellT.white],
  ];


  @override
  Map<FigureTeam, Map<FigureBaseType, List<Point>>> points = {
    FigureTeam.Black: {
      FigureBaseType.Pawn : <Point> [
        for(int i = 0; i < 8; ++i)
          Point(i, 1),
      ],
      FigureBaseType.Rock : <Point> [
        Point(0, 0),
        Point(7, 0),
      ],
      FigureBaseType.Bishop : <Point> [
        Point(2, 0),
        Point(5, 0),
      ],
      FigureBaseType.Knight : <Point> [
        Point(1, 0),
        Point(6, 0),
      ],
      FigureBaseType.King : <Point> [
        Point(4, 0),
      ],
      FigureBaseType.Queen : <Point> [
        Point(3, 0),
      ],

    },

    FigureTeam.White: {
      FigureBaseType.Pawn : <Point> [
        for(int i = 0; i < 8; ++i)
          Point(i, 6),
      ],
      FigureBaseType.Rock : <Point> [
        Point(0, 7),
        Point(7, 7),
      ],
      FigureBaseType.Bishop : <Point> [
        Point(2, 7),
        Point(5, 7),
      ],
      FigureBaseType.Knight : <Point> [
        Point(1, 7),
        Point(6, 7),
      ],
      FigureBaseType.King : <Point> [
        Point(4, 7),
      ],
      FigureBaseType.Queen : <Point> [
        Point(3, 7),
      ],

    },
  };

  @override
  int maxH = 8;

  @override
  int maxW = 8;



}
