

import 'dart:ffi';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:rpgchessgame/models/FieldTemplate.dart';
import 'package:rpgchessgame/models/GameConfig.dart';
import 'package:rpgchessgame/models/figures/FigureBase.dart';

import 'Cell.dart';
import 'Player.dart';
import 'abilities/AbilityBase.dart';

enum MoveT {
  can,
  atack,
  cannot,
  figure,
}


class Field {
  int maxW, maxH;
  Point selectedFigure;
  List<List<Cell>> field = List<List<Cell>>();
  //Map<FigureTeam, IBuff> buffs;
  //Map<FigureTeam, IDeBuff> debuffs;

  List<List<MoveT>> movesMap;

  Field.fromTemplate({
    @required FieldTemplate template,
    @required List<Player> players,
  }) {
    maxH = template.maxH;
    maxW = template.maxW;

    movesMap = [
      for(int i = 0; i < maxH; ++i)
        List.filled(maxW, MoveT.cannot),
    ];

    for(int y = 0; y < maxH; ++y) {
      field.add(List<Cell>());
      for(int x = 0; x < maxW; ++x) {
        switch(template.map[y][x]) {
          case CellT.black:
            field[y].add(Cell(texture: _getRandBlackCell()));
            break;
          case CellT.white:
            field[y].add(Cell(texture: _getRandWhiteCell()));
            break;
          case CellT.empty:
            field[y].add(Cell.empty(texture: _getRandBlackCell()));
            break;
        }
      }
    }

    for(int i = 0; i < players.length; ++i) {
      for(var type in FigureBaseType.values) {
        var pointList = template.points[players[i].teamId][type];
        for(int ii = 0; ii < pointList.length; ++ii) {
          field[pointList[ii].y][pointList[ii].x].figure =
              players[i].figures[type][ii];
        }
      }
    }
  }

  Widget drawField() {
    return GridView.count(
      crossAxisCount: maxW,
      physics: NeverScrollableScrollPhysics(),
      scrollDirection: Axis.vertical,
      padding: EdgeInsets.all(0),
      children: <Widget>[
        for(int y = 0; y < maxH; ++y)
          for(int x = 0; x < maxW; ++x)
            field[y][x].drawCell(_getSelectedCellImage(x, y)),



      ],
    );
  }

  Widget _getSelectedCellImage(int x, int y) {
    switch(movesMap[y][x]) {
      case MoveT.can:
        return Image.asset("assets/game/green.png");
      case MoveT.atack:
        return Image.asset("assets/game/red.png");
      case MoveT.cannot:
        return Opacity(
          child: Image.asset("assets/game/green.png"),
          opacity: 0.0,
        );
      case MoveT.figure:
        return Image.asset("assets/game/blue.png");
        break;
    }

  }


  
}

Image _getRandBlackCell() {
  return Image.asset(
      ProvideGameConfig.getInst().wayToBlackCell
          + (Random().nextInt(8) + 1).toString() + ".jpg"
  );
}


Image _getRandWhiteCell() {
  return Image.asset(
      ProvideGameConfig.getInst().wayToWhiteCell
          + (Random().nextInt(8) + 1).toString() + ".jpg"
  );
}