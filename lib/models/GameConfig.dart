

class _GameConfig {
  int playersCount = 2;
  String wayToGameBackground = "assets/gamebackgrounds/black_fone.jpg";
  String wayToBlackCell = "assets/cells/marble_black/";
  String wayToWhiteCell = "assets/cells/beaten_white/";

  DateTime timeOfGame = DateTime(0, 1, 1, 0, 15, 0, 0, 0);

}

class ProvideGameConfig {
  static _GameConfig _instance;

  static _GameConfig getInst() {
    if (_instance == null)
      _instance = _GameConfig();

    return _instance;
  }
}