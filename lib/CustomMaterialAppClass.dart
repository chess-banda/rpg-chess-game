import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'AbilitiesLocalizations.dart';
import 'FigureLocalizations.dart';
import 'Localization.dart';


class LocalizedLandscapeMaterialApp extends StatelessWidget {

  final GlobalKey<NavigatorState> navigatorKey;
  final Widget home;
  final RouteFactory onUnknownRoute;
  final List<NavigatorObserver> navigatorObservers;
  final TransitionBuilder builder;
  final String title;
  final GenerateAppTitle onGenerateTitle;
  final ThemeData theme;
  final ThemeData darkTheme;
  final ThemeMode themeMode;
  final Color color;
  final Locale locale;
  final Iterable<LocalizationsDelegate<dynamic>> localizationsDelegates;
  final LocaleListResolutionCallback localeListResolutionCallback;
  final LocaleResolutionCallback localeResolutionCallback;
  final Iterable<Locale> supportedLocales;
  final bool showPerformanceOverlay;
  final bool checkerboardRasterCacheImages;
  final bool checkerboardOffscreenLayers;
  final bool showSemanticsDebugger;
  final bool debugShowCheckedModeBanner;
  final bool debugShowMaterialGrid;
  final StatelessWidget statelessWidget;
  final StatefulWidget statefulWidget;

  const LocalizedLandscapeMaterialApp({
    Key key,
    this.statelessWidget,
    this.statefulWidget,
    this.navigatorKey,
    this.home,
    this.onUnknownRoute,
    this.navigatorObservers = const <NavigatorObserver>[],
    this.builder,
    this.title = '',
    this.onGenerateTitle,
    this.color,
    this.theme,
    this.darkTheme,
    this.themeMode = ThemeMode.system,
    this.locale,
    this.localizationsDelegates = const [
      const GLocalizationsDelegate(),
      const FigureLocalizationsDelegate(),
      const AbilitiesLocalizationsDelegate(),
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ],
    this.localeListResolutionCallback,
    this.localeResolutionCallback,
    this.supportedLocales = const <Locale>[
      Locale('en', ''),
      Locale('ru', '')
    ],
    this.debugShowMaterialGrid = false,
    this.showPerformanceOverlay = false,
    this.checkerboardRasterCacheImages = false,
    this.checkerboardOffscreenLayers = false,
    this.showSemanticsDebugger = false,
    this.debugShowCheckedModeBanner = true,
  })
      : assert(navigatorObservers != null),
        assert(title != null),
        assert(debugShowMaterialGrid != null),
        assert(showPerformanceOverlay != null),
        assert(checkerboardRasterCacheImages != null),
        assert(checkerboardOffscreenLayers != null),
        assert(showSemanticsDebugger != null),
        assert(debugShowCheckedModeBanner != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome
        .setPreferredOrientations([DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
    SystemChrome
        .setEnabledSystemUIOverlays([]);
    return MaterialApp(
        key: key,
        navigatorObservers: navigatorObservers,
        navigatorKey: navigatorKey,
        localizationsDelegates: localizationsDelegates,
        localeResolutionCallback: localeResolutionCallback,
        localeListResolutionCallback: localeListResolutionCallback,
        debugShowMaterialGrid: debugShowMaterialGrid,
        debugShowCheckedModeBanner: debugShowCheckedModeBanner,
        darkTheme: darkTheme,
        checkerboardRasterCacheImages: checkerboardRasterCacheImages,
        checkerboardOffscreenLayers: checkerboardOffscreenLayers,
        color: color,
        builder: builder,
        onGenerateTitle: onGenerateTitle,
        onUnknownRoute: onUnknownRoute,
        home: home,
        showPerformanceOverlay: showPerformanceOverlay,
        showSemanticsDebugger: showSemanticsDebugger,
        supportedLocales: supportedLocales,
        theme: theme,
        themeMode: themeMode,
        title: title
    );
  }

}